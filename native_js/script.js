var readmore = {
    config: {
        wrapper: '.js_readmore',
        textblock: '.js_text',
        contentblock: '.js_content',
        more: '.js_more',
        openText: 'Open',
        closeText: 'Close',
        height: 300,
    },
    initialize: function () {
        var $this = this;
        $('body').on('click', this.config.wrapper + ' ' + this.config.more, function () {
            $this.run(this);
        });
        if ($(this.config.wrapper).length > 0) {
            $(this.config.wrapper).each(function () {
                var heightOrigin = $this.getHeightOrigin(this);
                var heightClose = $this.getHeightClose(this);
                if (heightOrigin >= heightClose) {
                    $this.run(this);
                } else {
                    $(this).find($this.config.more).hide();
                }
            });
        }
    },
    run: function (elem) {
        var wrapper = $(elem).closest(this.config.wrapper);
        var status = $(wrapper).attr('data-status');
        if (status == 'close') {
            this.open(wrapper);
        } else {
            this.close(wrapper);
        }
    },
    open: function (elem) {
        var height = this.getHeightOrigin(elem);
        $(elem).attr({'data-status': 'open'});
        $(elem).find(this.config.textblock).animate({height: this.getHeightOrigin(elem)});
        $(elem).find(this.config.more).html(this.getText(elem, 'close'));
    },
    close: function (elem) {
        var height = this.getHeightClose(elem);
        $(elem).attr({'data-status': 'close'});
        $(elem).find(this.config.textblock).animate({height: this.getHeightClose(elem)});
        $(elem).find(this.config.more).html(this.getText(elem, 'open'));
    },
    getHeightClose: function (elem) {
        var height = $(elem).attr('data-height');
        if (!height) {
            height = this.config.height;
        }
        return height;
    },
    getHeightOrigin: function (elem) {
        return $(elem).find(this.config.contentblock).height();
    },
    getText: function (elem, type) {
        var text = $(elem).attr('data-' + type);
        if (!text) {
            text = this.config[type + 'Text'];
        }
        return text;
    }
};
readmore.initialize();