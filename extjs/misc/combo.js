msMotivation.utils.renderBoolean = function (value, props, row) {
	return value
		? String.format('<span class="green">{0}</span>', _('yes'))
		: String.format('<span class="red">{0}</span>', _('no'));
};

msMotivation.utils.getMenu = function (actions, grid, selected) {
	var menu = [];
	var cls, icon, title, action = '';

	for (var i in actions) {
		if (!actions.hasOwnProperty(i)) {
			continue;
		}

		var a = actions[i];
		if (!a['menu']) {
			if (a == '-') {
				menu.push('-');
			}
			continue;
		}
		else if (menu.length > 0 && /^remove/i.test(a['action'])) {
			menu.push('-');
		}

		if (selected.length > 1) {
			if (!a['multiple']) {
				continue;
			}
			else if (typeof(a['multiple']) == 'string') {
				a['title'] = a['multiple'];
			}
		}

		cls = a['cls'] ? a['cls'] : '';
		icon = a['icon'] ? a['icon'] : '';
		title = a['title'] ? a['title'] : a['title'];
		action = a['action'] ? grid[a['action']] : '';

		menu.push({
			handler: action,
			text: String.format(
				'<span class="{0}"><i class="x-menu-item-icon {1}"></i>{2}</span>',
				cls, icon, title
			),
		});
	}

	return menu;
};

msMotivation.utils.renderActions = function (value, props, row) {
	var res = [];
	var cls, icon, title, action, item = '';
	for (var i in row.data.actions) {
		if (!row.data.actions.hasOwnProperty(i)) {
			continue;
		}
		var a = row.data.actions[i];
		if (!a['button']) {
			continue;
		}

		cls = a['cls'] ? a['cls'] : '';
		icon = a['icon'] ? a['icon'] : '';
		action = a['action'] ? a['action'] : '';
		title = a['title'] ? a['title'] : '';

		item = String.format(
			'<li class="{0}"><button class="btn btn-default {1}" action="{2}" title="{3}"></button></li>',
			cls, icon, action, title
		);

		res.push(item);
	}

	return String.format(
		'<ul class="msmotivation-row-actions">{0}</ul>',
		res.join('')
	);
};

msMotivation.combo.ConditionLogicsList = function(config) {
	config = config || {};
	Ext.applyIf(config,{
		id: 'motivation-combo-condition-logic-list'
		,fieldLabel: 'ConditionLogicsList'
		,fields: ['id','name']
		,valueField: 'id'
		,displayField: 'name'
		,hiddenName: 'logic'
		,url: msMotivation.config.connector_url
		,baseParams: {
			action: 'mgr/condition/getlogicslist'
			,combo: 1
			,id: config.value
		}
		,pageSize: 20
                ,paging: true
		,emptyText: 'select logic'
		,editable: true
	});
	msMotivation.combo.ConditionLogicsList.superclass.constructor.call(this,config);
};
Ext.extend(msMotivation.combo.ConditionLogicsList,MODx.combo.ComboBox);
Ext.reg('motivation-combo-condition-logic-list',msMotivation.combo.ConditionLogicsList);

msMotivation.combo.BadgeLogicsList = function(config) {
	config = config || {};
	Ext.applyIf(config,{
		id: 'motivation-combo-badge-logic-list'
		,fieldLabel: 'BadgeLogicsList'
		,fields: ['id','name']
		,valueField: 'id'
		,displayField: 'name'
		,hiddenName: 'logic'
		,url: msMotivation.config.connector_url
		,baseParams: {
			action: 'mgr/badge/getlogicslist'
			,combo: 1
			,id: config.value
		}
		,pageSize: 20
                ,paging: true
		,emptyText: 'select logic'
		,editable: true
	});
	msMotivation.combo.BadgeLogicsList.superclass.constructor.call(this,config);
};
Ext.extend(msMotivation.combo.BadgeLogicsList,MODx.combo.ComboBox);
Ext.reg('motivation-combo-badge-logic-list',msMotivation.combo.BadgeLogicsList);

msMotivation.combo.Elements = function(config) {
	config = config || {};
	Ext.applyIf(config,{
                id: 'motivation-combo-elements'
		,fieldLabel: 'Elements'
		,fields: ['id','pagetitle']
		,valueField: 'id'
		,displayField: 'pagetitle'
                ,name: 'element'	
		,url: msMotivation.config.connector_url
		,baseParams: {
			action: 'mgr/condition/getelementslist'	
                        ,ids: ''                        
		}                
		,emptyText: 'select elements'
		,tpl: new Ext.XTemplate(''
                    +'<tpl for="."><div class="x-combo-list-item motivation-list-item">'    
                    +'<span><small>({id})</small> <b>{pagetitle}</b></span>'
                    +'</div></tpl>',{
                            compiled: true
                    }
                )
		,itemSelector: 'div.motivation-list-item'
		,pageSize: 20
                ,paging: true
                ,typeAhead: true
		,editable: true
		,allowBlank: true
                ,listeners: {
                            beforequery: {
                                fn: function() {                                    
                                    var grid = Ext.getCmp('motivation-grid-elements');
                                    var s = grid.getStore();
                                    var allRecords = s.snapshot || s.data;
                                    var ids = [];
                                    allRecords.each(function(record) {
                                        ids.push(record.data.unique_id);
                                    }); 
                                    
                                    this.store.baseParams.ids = JSON.stringify(ids); 
                                },scope:this
                            }                    
                            ,select: function(box, record, index) {
                                box.store.remove(record);                               
                               
                                var grid = Ext.getCmp('motivation-grid-elements');
                                var s = grid.getStore();
                                
                                var RecordTemplate = Ext.data.Record.create([
                                    {name: 'unique_id', mapping: 'unique_id'}
                                    ,{name: 'pagetitle', mapping: 'pagetitle'}
                                    ,{name: 'value', mapping: 'value'}
                                ]);

                                var record = new RecordTemplate({
                                    unique_id: record.data.id
                                    ,pagetitle: record.data.pagetitle
                                    ,value: 1
                                });
                                
                                s.add(record);                                  
                            }                            
                        }		
	});
	msMotivation.combo.Elements.superclass.constructor.call(this,config);
};
Ext.extend(msMotivation.combo.Elements,MODx.combo.ComboBox);
Ext.reg('motivation-combo-elements',msMotivation.combo.Elements);

msMotivation.grid.GridElements = function(config) {
    config = config || {};
    Ext.applyIf(config,{
        title: _('roles')
        ,id: 'motivation-grid-elements'
        ,url: msMotivation.config.connector_url
        ,baseParams: {
            action: ''           
        }
        ,fields: ['id','unique_id','pagetitle', 'value']
        ,paging: true
        ,pageSize:5
        ,autosave: false
        ,columns: [{
            header: _('id')
            ,dataIndex: 'id'
            ,width: 30
            ,sortable: false
            ,hidden:true
        },{
            header: 'unique_id'
            ,dataIndex: 'unique_id'
            ,width: 30
            ,sortable: false
            ,hidden:true
        },{
            header: 'Название'
            ,dataIndex: 'pagetitle'
            ,width: 150
            ,sortable: true
        },{
            header: _('value')
            ,dataIndex: 'value'
            ,width: 150
            ,sortable: true
            ,editor: { xtype: 'textfield', allowBlank: false }
            ,renderer: Ext.util.Format.htmlEncode
        }]
    });
    msMotivation.grid.GridElements.superclass.constructor.call(this,config);
    this.on('afterrender',this._updateComboElements,this);
};

Ext.extend(msMotivation.grid.GridElements,MODx.grid.Grid,{
    	        
        getMenu: function(g,ri,e) {
            var m = [];
            var $this = this;
                      
            m.push({
                text: 'Удалить'
                ,handler: function() {
                    Ext.MessageBox.confirm(_(''), 'Удалить элемент?', function(btnId) {
                        if (btnId == 'yes') {
                            $this.removeElement(ri);    
                        }
                    });                    
                },
                scope: this
            });
            return m;
        }
        ,removeElement: function(ri){
            var store = this.getStore(); 
            store.removeAt(ri);
            this._updateComboElements(); 
        }
        ,_updateComboElements: function(){           
            var store = this.getStore();
            var allRecords = store.snapshot || store.data;
            var ids = [];
            allRecords.each(function(record) {
                ids.push(record.data.unique_id);
            });  
            var combo = Ext.getCmp('motivation-combo-elements');
            combo.store.baseParams.ids = JSON.stringify(ids);
            combo.store.load();
        }        
    
});
Ext.reg('motivation-grid-elements',msMotivation.grid.GridElements);







msMotivation.combo.Items = function(config) {
	config = config || {};
	Ext.applyIf(config,{
		xtype:'superboxselect'
		,allowBlank: true
		,msgTarget: 'under'
		,allowAddNewData: true
		,addNewDataOnBlur : true
		,resizable: true
		,name: config.name || 'items'
		,anchor:'99%'
		,minChars: 2                
		,store:new Ext.data.JsonStore({
			id: (config.name || 'items') + '-list'
			,root:'results'
			,autoLoad: true
			,autoSave: false
			,totalProperty:'total'
                        ,fields: ['id','name']
			,url: msMotivation.config.connector_url
			,baseParams: {
                            action: config.action || 'mgr/item/getlist'
                            ,combo: 1
			}
		})		
		,valueField: 'id'
		,displayField: 'name'
                ,tpl: new Ext.XTemplate(''
                    +'<tpl for="."><div class="x-combo-list-item motivation-list-item">'    
                    +'<span><small>({id})</small> <b>{name}</b></span>'
                    +'</div></tpl>',{
                            compiled: true
                    }
                )
		,triggerAction: 'all'
		,extraItemCls: 'x-tag'
		,expandBtnCls: MODx.modx23 ? 'x-form-trigger' : 'x-superboxselect-btn-expand'
		,clearBtnCls: MODx.modx23 ? 'x-form-trigger' : 'x-superboxselect-btn-clear'
                ,renderTo: Ext.getBody()

	});
	config.name += '[]';
	msMotivation.combo.Items.superclass.constructor.call(this,config);
};
Ext.extend(msMotivation.combo.Items,Ext.ux.form.SuperBoxSelect);
Ext.reg('motivation-supercombo-items',msMotivation.combo.Items);