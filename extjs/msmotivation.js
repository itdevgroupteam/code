var msMotivation = function (config) {
	config = config || {};
	msMotivation.superclass.constructor.call(this, config);
};
Ext.extend(msMotivation, Ext.Component, {
	page: {}, window: {}, grid: {}, tree: {}, panel: {}, combo: {}, config: {}, view: {}, utils: {}
});
Ext.reg('msmotivation', msMotivation);

msMotivation = new msMotivation();