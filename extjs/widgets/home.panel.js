msMotivation.panel.Home = function (config) {
	config = config || {};
	Ext.apply(config, {
		baseCls: 'modx-formpanel',
		layout: 'anchor',
		/*
		 stateful: true,
		 stateId: 'msmotivation-panel-home',
		 stateEvents: ['tabchange'],
		 getState:function() {return {activeTab:this.items.indexOf(this.getActiveTab())};},
		 */
		hideMode: 'offsets',
		items: [{
			html: '<h2>' + _('msmotivation') + '</h2>',
			cls: '',
			style: {margin: '15px 0'}
		}, {
			xtype: 'modx-tabs',
			defaults: {border: false, autoHeight: true},
			border: true,
			hideMode: 'offsets',
			items: [{
				title: _('msmotivation_badges'),
				layout: 'anchor',
				items: [{
					html: _('msmotivation_intro_msg'),
					cls: 'panel-desc',
				}, {
					xtype: 'msmotivation-grid-badges',
					cls: 'main-wrapper',
				}]
			},{
				title: _('msmotivation_conditions'),
				layout: 'anchor',
				items: [{
					html: _('msmotivation_intro_msg'),
					cls: 'panel-desc',
				}, {
					xtype: 'msmotivation-grid-conditions',
					cls: 'main-wrapper',
				}]
			},{
				title: _('msmotivation_presents'),
				layout: 'anchor',
				items: [{
					html: _('msmotivation_intro_msg'),
					cls: 'panel-desc',
				}, {
					xtype: 'msmotivation-grid-presents',
					cls: 'main-wrapper',
				}]
			}]
		}]
	});
	msMotivation.panel.Home.superclass.constructor.call(this, config);
};
Ext.extend(msMotivation.panel.Home, MODx.Panel);
Ext.reg('msmotivation-panel-home', msMotivation.panel.Home);
