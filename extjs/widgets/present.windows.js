msMotivation.window.CreatePresent = function (config) {
	config = config || {};
	if (!config.id) {
		config.id = 'msmotivation-present-window-create';
	}
	Ext.applyIf(config, {
		title: _('msmotivation_present_create'),
		width: 550,
		autoHeight: true,
		url: msMotivation.config.connector_url,
		action: 'mgr/present/create',
		fields: this.getFields(config),
		keys: [{
			key: Ext.EventObject.ENTER, shift: true, fn: function () {
				this.submit()
			}, scope: this
		}]
	});
	msMotivation.window.CreatePresent.superclass.constructor.call(this, config);
};
Ext.extend(msMotivation.window.CreatePresent, MODx.Window, {

	getFields: function (config) {
		return [{
			xtype: 'textfield',
			fieldLabel: _('msmotivation_present_name'),
			name: 'name',
			id: config.id + '-name',
			anchor: '99%',
			allowBlank: false,
		},{
			xtype: 'modx-combo-browser',
			fieldLabel: _('msmotivation_present_img'),
			name: 'img',
			id: config.id + '-img',
			anchor: '99%',
			height: 150,
		},{
			xtype: 'motivation-supercombo-items',
			fieldLabel: _('msmotivation_badges'),
			name: 'badges',
                        id: config.id + '-badges',
			anchor: '99%',	
                        action: 'mgr/badge/getlist'
		}/*, {
			xtype: 'xcheckbox',
			boxLabel: _('msmotivation_item_active'),
			name: 'active',
			id: config.id + '-active',
			checked: true,
		}*/];
	},        
        
});
Ext.reg('msmotivation-present-window-create', msMotivation.window.CreatePresent);


msMotivation.window.UpdatePresent = function (config) {
	config = config || {};
	if (!config.id) {
		config.id = 'msmotivation-present-window-update';
	}
	Ext.applyIf(config, {
		title: _('msmotivation_present_update'),
		width: 550,
		autoHeight: true,
		url: msMotivation.config.connector_url,
		action: 'mgr/present/update',
		fields: this.getFields(config),
		keys: [{
			key: Ext.EventObject.ENTER, shift: true, fn: function () {
				this.submit()
			}, scope: this
		}]
	});
	msMotivation.window.UpdatePresent.superclass.constructor.call(this, config);
};
Ext.extend(msMotivation.window.UpdatePresent, MODx.Window, {

	getFields: function (config) {
		return [{
			xtype: 'hidden',
			name: 'id',
			id: config.id + '-id',
		}, {
			xtype: 'textfield',
			fieldLabel: _('msmotivation_present_name'),
			name: 'name',
			id: config.id + '-name',
			anchor: '99%',
			allowBlank: false,
		}, {
			xtype: 'modx-combo-browser',
			fieldLabel: _('msmotivation_present_img'),
			name: 'img',
			id: config.id + '-img',
			anchor: '99%',
			height: 150,
		},{
			xtype: 'motivation-supercombo-items',
			fieldLabel: _('msmotivation_badges'),
			name: 'badges',
                        id: config.id + '-badges',
			anchor: '99%',	
                        action: 'mgr/badge/getlist'
		}/*, {
			xtype: 'xcheckbox',
			boxLabel: _('msmotivation_item_active'),
			name: 'active',
			id: config.id + '-active',
			
		}*/];
	},
	

});
Ext.reg('msmotivation-present-window-update', msMotivation.window.UpdatePresent);