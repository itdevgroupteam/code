msMotivation.grid.Conditions = function (config) {
	config = config || {};
	if (!config.id) {
		config.id = 'msmotivation-grid-conditions';
	}
	Ext.applyIf(config, {
		url: msMotivation.config.connector_url,
		fields: this.getFields(config),
		columns: this.getColumns(config),
		tbar: this.getTopBar(config),
		sm: new Ext.grid.CheckboxSelectionModel(),
		baseParams: {
			action: 'mgr/condition/getlist'
		},
		listeners: {
			rowDblClick: function (grid, rowIndex, e) {
				var row = grid.store.getAt(rowIndex);
				this.updateCondition(grid, e, row);
			}
		},
		viewConfig: {
			forceFit: true,
			enableRowBody: true,
			autoFill: true,
			showPreview: true,
			scrollOffset: 0,
			getRowClass: function (rec, ri, p) {                                
				return false == rec.data.active
					? 'msmotivation-grid-row-disabled'
					: '';
			}
		},
		paging: true,
		remoteSort: true,
		autoHeight: true,
	});
	msMotivation.grid.Conditions.superclass.constructor.call(this, config);

	// Clear selection on grid refresh
	this.store.on('load', function () {
		if (this._getSelectedIds().length) {
			this.getSelectionModel().clearSelections();
		}
	}, this);
};
Ext.extend(msMotivation.grid.Conditions, MODx.grid.Grid, {
	windows: {},

	getMenu: function (grid, rowIndex) {
		var ids = this._getSelectedIds();

		var row = grid.getStore().getAt(rowIndex);
		var menu = msMotivation.utils.getMenu(row.data['actions'], this, ids);

		this.addContextMenuItem(menu);
	},

	createCondition: function (btn, e) {           
                var w = Ext.getCmp('msmotivation-condition-window');
                if (w) {w.hide().getEl().remove();}
		w = MODx.load({
			xtype: 'msmotivation-condition-window',
			id: 'msmotivation-condition-window',
                        update: 0, 
                        record: {'object':{'elements':{'results': {}, 'total': 0 }}},
                        baseParams: {
				action: 'mgr/condition/create'
			},
			listeners: {
				success: {fn: function () {this.refresh();}, scope: this}
                                ,hide: {fn: function() {this.getEl().remove();}}
			}
		});
		w.reset();
		w.setValues({active: true});
		w.show(e.target);
	},

	updateCondition: function (btn, e, row) {
		if (typeof(row) != 'undefined') {
			this.menu.record = row.data;
		}
		else if (!this.menu.record) {
			return false;
		}
		var id = this.menu.record.id;

		MODx.Ajax.request({
			url: this.config.url,
			params: {
				action: 'mgr/condition/get',
				id: id
			},
			listeners: {
				success: {
					fn: function (r) {                                             
                                                var w = Ext.getCmp('msmotivation-condition-window');
                                                if (w) {w.hide().getEl().remove();}
						w = MODx.load({
							xtype: 'msmotivation-condition-window',
							id: 'msmotivation-condition-window',
							record: r,
                                                        update: 1,     
							listeners: {
								success: {fn: function () {this.refresh();}, scope: this}                                                                
								,hide: {fn: function() {this.getEl().remove();}}
							}
						});
						w.reset();
						w.setValues(r.object);
						w.show(e.target);
					}, scope: this
				}
			}
		});
	},

	removeCondition: function (act, btn, e) {
		var ids = this._getSelectedIds();
		if (!ids.length) {
			return false;
		}
		MODx.msg.confirm({
			title: ids.length > 1
				? _('msmotivation_conditions_remove')
				: _('msmotivation_condition_remove'),
			text: ids.length > 1
				? _('msmotivation_conditions_remove_confirm')
				: _('msmotivation_condition_remove_confirm'),
			url: this.config.url,
			params: {
				action: 'mgr/condition/remove',
				ids: Ext.util.JSON.encode(ids),
			},
			listeners: {
				success: {
					fn: function (r) {
						this.refresh();
					}, scope: this
				}
			}
		});
		return true;
	},

	disableCondition: function (act, btn, e) {            
		var ids = this._getSelectedIds();
		if (!ids.length) {
			return false;
		} 
		MODx.Ajax.request({
			url: this.config.url,
			params: {
				action: 'mgr/condition/disable',
				ids: Ext.util.JSON.encode(ids),
			},
			listeners: {
				success: {
					fn: function () {
						this.refresh();
					}, scope: this
				}
			}
		})                
	},

	enableCondition: function (act, btn, e) {
		var ids = this._getSelectedIds();
		if (!ids.length) {
			return false;
		}
		MODx.Ajax.request({
			url: this.config.url,
			params: {
				action: 'mgr/condition/enable',
				ids: Ext.util.JSON.encode(ids),
			},
			listeners: {
				success: {
					fn: function () {
						this.refresh();
					}, scope: this
				}
			}
		})
	},

	getFields: function (config) {
		return ['id','name', 'logic', 'active','actions'];
	},

	getColumns: function (config) {
		return [{
			header: _('msmotivation_condition_id'),
			dataIndex: 'id',
			sortable: true,
			width: 50
		}, {
			header: _('msmotivation_condition_name'),
			dataIndex: 'name',                       
			sortable: true,
			width: 100,
		}, {
			header: _('msmotivation_logic'),
			dataIndex: 'logic',                        
			sortable: true,
			width: 50,
		}, {
			header: _('msmotivation_grid_actions'),
			dataIndex: 'actions',
			renderer: msMotivation.utils.renderActions,
			sortable: false,
			width: 50,
			id: 'actions'
		}];
	} 
        
	,getTopBar: function (config) {
		return [{
			text: '<i class="icon icon-plus"></i>&nbsp;' + _('msmotivation_condition_create'),
			handler: this.createCondition,
			scope: this
		}];
	},

	onClick: function (e) {
		var elem = e.getTarget();
		if (elem.nodeName == 'BUTTON') {
			var row = this.getSelectionModel().getSelected();
			if (typeof(row) != 'undefined') {
				var action = elem.getAttribute('action');
				if (action == 'showMenu') {
					var ri = this.getStore().find('id', row.id);
					return this._showMenu(this, ri, e);
				}
				else if (typeof this[action] === 'function') {
					this.menu.record = row.data;
					return this[action](this, e);
				}
			}
		}
		return this.processEvent('click', e);
	},

	_getSelectedIds: function () {
		var ids = [];
		var selected = this.getSelectionModel().getSelections();

		for (var i in selected) {
			if (!selected.hasOwnProperty(i)) {
				continue;
			}
			ids.push(selected[i]['id']);
		}

		return ids;
	}
});
Ext.reg('msmotivation-grid-conditions', msMotivation.grid.Conditions);
