msMotivation.grid.Presents = function (config) {
	config = config || {};
	if (!config.id) {
		config.id = 'msmotivation-grid-presents';
	}
	Ext.applyIf(config, {
		url: msMotivation.config.connector_url,
		fields: this.getFields(config),
		columns: this.getColumns(config),
		tbar: this.getTopBar(config),
		sm: new Ext.grid.CheckboxSelectionModel(),
		baseParams: {
			action: 'mgr/present/getlist'
		},
		listeners: {
			rowDblClick: function (grid, rowIndex, e) {
				var row = grid.store.getAt(rowIndex);
				this.updatePresent(grid, e, row);
			}
		},
		viewConfig: {
			forceFit: true,
			enableRowBody: true,
			autoFill: true,
			showPreview: true,
			scrollOffset: 0,
			getRowClass: function (rec, ri, p) {                                
				return false == rec.data.active
					? 'msmotivation-grid-row-disabled'
					: '';
			}
		},
		paging: true,
		remoteSort: true,
		autoHeight: true,
	});
	msMotivation.grid.Presents.superclass.constructor.call(this, config);

	// Clear selection on grid refresh
	this.store.on('load', function () {
		if (this._getSelectedIds().length) {
			this.getSelectionModel().clearSelections();
		}
	}, this);
};
Ext.extend(msMotivation.grid.Presents, MODx.grid.Grid, {
	windows: {},

	getMenu: function (grid, rowIndex) {
		var ids = this._getSelectedIds();

		var row = grid.getStore().getAt(rowIndex);
		var menu = msMotivation.utils.getMenu(row.data['actions'], this, ids);

		this.addContextMenuItem(menu);
	},

	createPresent: function (btn, e) {
                var w = Ext.getCmp('msmotivation-present-window-create');
                if (w) {w.hide().getEl().remove();}
		w = MODx.load({
			xtype: 'msmotivation-present-window-create',
			id: Ext.id(),
			listeners: {
                                success: {fn: function () {this.refresh();}, scope: this}                                                                
                                ,hide: {fn: function() {this.getEl().remove();}}
                        }
		});
		w.reset();
		w.setValues({active: true});
		w.show(e.target);
	},

	updatePresent: function (btn, e, row) {
		if (typeof(row) != 'undefined') {
			this.menu.record = row.data;
		}
		else if (!this.menu.record) {
			return false;
		}
		var id = this.menu.record.id;

		MODx.Ajax.request({
			url: this.config.url,
			params: {
				action: 'mgr/present/get',
				id: id
			},
			listeners: {
				success: {
					fn: function (r) {
                                                var w = Ext.getCmp('msmotivation-present-window-update');
                                                if (w) {w.hide().getEl().remove();}
						w = MODx.load({
							xtype: 'msmotivation-present-window-update',
							id: Ext.id(),
							record: r,
							listeners: {
								success: {fn: function () {this.refresh();}, scope: this}                                                                
								,hide: {fn: function() {this.getEl().remove();}}
							}
						});
						w.reset();
						w.setValues(r.object);
						w.show(e.target);
					}, scope: this
				}
			}
		});
	},

	removePresent: function (act, btn, e) {
		var ids = this._getSelectedIds();
		if (!ids.length) {
			return false;
		}
		MODx.msg.confirm({
			title: ids.length > 1
				? _('msmotivation_presents_remove')
				: _('msmotivation_present_remove'),
			text: ids.length > 1
				? _('msmotivation_presents_remove_confirm')
				: _('msmotivation_present_remove_confirm'),
			url: this.config.url,
			params: {
				action: 'mgr/present/remove',
				ids: Ext.util.JSON.encode(ids),
			},
			listeners: {
				success: {
					fn: function (r) {
						this.refresh();
					}, scope: this
				}
			}
		});
		return true;
	},

	disablePresent: function (act, btn, e) {
		var ids = this._getSelectedIds();
		if (!ids.length) {
			return false;
		}
		MODx.Ajax.request({
			url: this.config.url,
			params: {
				action: 'mgr/present/disable',
				ids: Ext.util.JSON.encode(ids),
			},
			listeners: {
				success: {
					fn: function () {
						this.refresh();
					}, scope: this
				}
			}
		})
	},

	enablePresent: function (act, btn, e) {
		var ids = this._getSelectedIds();
		if (!ids.length) {
			return false;
		}
		MODx.Ajax.request({
			url: this.config.url,
			params: {
				action: 'mgr/present/enable',
				ids: Ext.util.JSON.encode(ids),
			},
			listeners: {
				success: {
					fn: function () {
						this.refresh();
					}, scope: this
				}
			}
		})
	},

	getFields: function (config) {
		return ['id', 'name', 'img','active','actions'];
	},

	getColumns: function (config) {
		return [{
			header: _('msmotivation_present_id'),
			dataIndex: 'id',
			sortable: true,
			width: 70
		}, {
			header: _('msmotivation_present_name'),
			dataIndex: 'name',
			sortable: true,
			width: 200,
		}, {
			header: _('msmotivation_present_img'),
			dataIndex: 'img',
                        renderer: this.renderImg,
			sortable: false,
			width: 250,
		}, {
			header: _('msmotivation_grid_actions'),
			dataIndex: 'actions',
			renderer: msMotivation.utils.renderActions,
			sortable: false,
			width: 100,
			id: 'actions'
		}];
	},
        
        renderImg: function(value) {
		if (value) {
			return '<img src="/' + value + '" height="61" style="display:block;margin:auto;"/>';
		}
		else {
			return '';
		}
	}
        
        
	,getTopBar: function (config) {
		return [{
			text: '<i class="icon icon-plus"></i>&nbsp;' + _('msmotivation_present_create'),
			handler: this.createPresent,
			scope: this
		}];
	},

	onClick: function (e) {
		var elem = e.getTarget();
		if (elem.nodeName == 'BUTTON') {
			var row = this.getSelectionModel().getSelected();
			if (typeof(row) != 'undefined') {
				var action = elem.getAttribute('action');
				if (action == 'showMenu') {
					var ri = this.getStore().find('id', row.id);
					return this._showMenu(this, ri, e);
				}
				else if (typeof this[action] === 'function') {
					this.menu.record = row.data;
					return this[action](this, e);
				}
			}
		}
		return this.processEvent('click', e);
	},

	_getSelectedIds: function () {
		var ids = [];
		var selected = this.getSelectionModel().getSelections();

		for (var i in selected) {
			if (!selected.hasOwnProperty(i)) {
				continue;
			}
			ids.push(selected[i]['id']);
		}

		return ids;
	},

	_doSearch: function (tf, nv, ov) {
		this.getStore().baseParams.query = tf.getValue();
		this.getBottomToolbar().changePage(1);
		this.refresh();
	},

	_clearSearch: function (btn, e) {
		this.getStore().baseParams.query = '';
		Ext.getCmp(this.config.id + '-search-field').setValue('');
		this.getBottomToolbar().changePage(1);
		this.refresh();
	}
});
Ext.reg('msmotivation-grid-presents', msMotivation.grid.Presents);
