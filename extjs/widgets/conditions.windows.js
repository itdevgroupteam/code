msMotivation.window.Condition = function (config) {
	config = config || {};
	if (!config.id) {
		config.id = 'msmotivation-condition-window';
	}
	Ext.applyIf(config, {
		title: config.update ? _('msmotivation_condition_update') : _('msmotivation_condition_create'),
		width: 550,
		autoHeight: true,
		url: msMotivation.config.connector_url,
		action: 'mgr/condition/update',
		fields: this.getFields(config),
		keys: [{
			key: Ext.EventObject.ENTER, shift: true, fn: function () {
				this.submit();
			}, scope: this
		}]
	});
	msMotivation.window.Condition.superclass.constructor.call(this, config);        
};
Ext.extend(msMotivation.window.Condition, MODx.Window, {

	getFields: function (config) {
		return [{
			xtype: 'hidden',
			name: 'id',
			id: config.id + '-id',
		}, {
			xtype: 'textfield',
			fieldLabel: _('msmotivation_condition_name'),
			name: 'name',			
			anchor: '99%',			
		}, {
			xtype: 'motivation-combo-elements',
			fieldLabel: _('msmotivation_condition_element'),
			name: 'element',			
			anchor: '99%',			
		},{
			xtype: 'motivation-grid-elements',
			fieldLabel: _('msmotivation_condition_elements'),
			name: 'list-elements',			
			anchor: '99%',
			height: 150,
                        listeners: { 
                            afterrender: function() {  
                                if(config.update){
                                    this.store.loadData(config.record.object.elements); 
                                }
                            }
                        }
		},{
			xtype: 'motivation-combo-condition-logic-list',
			fieldLabel: _('msmotivation_logic'),
			name: 'logic',
			id: config.id + '-logic',
			anchor: '99%',
			allowBlank: false,
		}/*, {
			xtype: 'xcheckbox',
			boxLabel: _('msmotivation_item_active'),
			name: 'active',
			id: config.id + '-active',
			checked: true,
		}*/];
	}
        
        ,submit: function(close) {
                        
            close = close === false ? false : true;
            var f = this.fp.getForm();
            var d = {};
            
            var grid = Ext.getCmp('motivation-grid-elements');
            if (grid) {
                var store = grid.getStore();
                var elements = [];

                store.each(function(rec){
                        elements.push(rec.data);
                    }
                );
                d.elements = JSON.stringify(elements);
            }
            Ext.apply(f.baseParams,d);
            
            if (f.isValid() && this.fireEvent('beforeSubmit',f.getValues())) {                
                f.submit({
                    waitMsg: _('saving')
                    ,submitEmptyText: this.config.submitEmptyText !== false
                    ,scope: this
                    ,failure: function(frm,a) {
                        if (this.fireEvent('failure',{f:frm,a:a})) {
                            MODx.form.Handler.errorExt(a.result,frm);
                        }
                        this.doLayout();
                    }
                    ,success: function(frm,a) {
                        if (this.config.success) {
                            Ext.callback(this.config.success,this.config.scope || this,[frm,a]);
                        }
                        this.fireEvent('success',{f:frm,a:a});
                        if (close) { this.config.closeAction !== 'close' ? this.hide() : this.close(); }
                        this.doLayout();
                    }
                });
            }
        }

});
Ext.reg('msmotivation-condition-window', msMotivation.window.Condition);