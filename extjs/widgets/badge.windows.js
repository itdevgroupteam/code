msMotivation.window.CreateBadge = function (config) {
	config = config || {};
	if (!config.id) {
		config.id = 'msmotivation-badge-window-create';
	}
	Ext.applyIf(config, {
		title: _('msmotivation_badge_create'),
		width: 550,
		autoHeight: true,
		url: msMotivation.config.connector_url,
		action: 'mgr/badge/create',
		fields: this.getFields(config),
		keys: [{
			key: Ext.EventObject.ENTER, shift: true, fn: function () {
				this.submit()
			}, scope: this
		}]
	});
	msMotivation.window.CreateBadge.superclass.constructor.call(this, config);
};
Ext.extend(msMotivation.window.CreateBadge, MODx.Window, {

	getFields: function (config) {
		return [{
			xtype: 'textfield',
			fieldLabel: _('msmotivation_badge_name'),
			name: 'name',
			id: config.id + '-name',
			anchor: '99%',
			allowBlank: false,
		},{
			xtype: 'modx-combo-browser',
			fieldLabel: _('msmotivation_badge_img'),
			name: 'img',
			id: config.id + '-img',
			anchor: '99%',
			allowBlank: false,
		},{
			xtype: 'motivation-supercombo-items',
			fieldLabel: _('msmotivation_conditions'),
			name: 'conditions',
			id: config.id + '-conditions',
			anchor: '99%',
			allowBlank: false,
                        action: 'mgr/condition/getlist'
		},{
			xtype: 'motivation-combo-badge-logic-list',
			fieldLabel: _('msmotivation_logic'),
			name: 'logic',
			id: config.id + '-logic_list',
			anchor: '99%',
			allowBlank: false,
		},{
			xtype: 'motivation-supercombo-items',
			fieldLabel: _('msmotivation_presents'),
			name: 'presents',
                        id: config.id + '-presents',
			anchor: '99%',	
                        action: 'mgr/present/getlist'
		}/*, {
			xtype: 'xcheckbox',
			boxLabel: _('msmotivation_item_active'),
			name: 'active',
			id: config.id + '-active',
			checked: true,
		}*/];
	}
});
Ext.reg('msmotivation-badge-window-create', msMotivation.window.CreateBadge);


msMotivation.window.UpdateBadge = function (config) {
	config = config || {};
	if (!config.id) {
		config.id = 'msmotivation-badge-window-update';
	}
	Ext.applyIf(config, {
		title: _('msmotivation_badge_update'),
		width: 550,
		autoHeight: true,
		url: msMotivation.config.connector_url,
		action: 'mgr/badge/update',
		fields: this.getFields(config),
		keys: [{
			key: Ext.EventObject.ENTER, shift: true, fn: function () {
				this.submit()
			}, scope: this
		}]
	});
	msMotivation.window.UpdateBadge.superclass.constructor.call(this, config);
};
Ext.extend(msMotivation.window.UpdateBadge, MODx.Window, {
	getFields: function (config) {
		return [{
			xtype: 'hidden',
			name: 'id',
			id: config.id + '-id',
		},{
			xtype: 'textfield',
			fieldLabel: _('msmotivation_badge_name'),
			name: 'name',
			id: config.id + '-name',
			anchor: '99%',
			allowBlank: false,
		},{
			xtype: 'modx-combo-browser',
			fieldLabel: _('msmotivation_badge_img'),
			name: 'img',
			id: config.id + '-img',
			anchor: '99%',
			allowBlank: false,
		},{
			xtype: 'motivation-supercombo-items',
			fieldLabel: _('msmotivation_conditions'),
			name: 'conditions',
			id: config.id + '-conditions',
			anchor: '99%',
			allowBlank: false,
                        action: 'mgr/condition/getlist'
		},{
			xtype: 'motivation-combo-badge-logic-list',
			fieldLabel: _('msmotivation_logic'),
			name: 'logic',
			id: config.id + '-logic_list',
			anchor: '99%',
			allowBlank: false,
		},{
			xtype: 'motivation-supercombo-items',
			fieldLabel: _('msmotivation_presents'),
			name: 'presents',
                        id: config.id + '-presents',
			anchor: '99%',	
                        action: 'mgr/present/getlist'
		}/*, {
			xtype: 'xcheckbox',
			boxLabel: _('msmotivation_item_active'),
			name: 'active',
			id: config.id + '-active',			
		}*/];
	}
});
Ext.reg('msmotivation-badge-window-update', msMotivation.window.UpdateBadge);