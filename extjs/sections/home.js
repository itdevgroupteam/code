msMotivation.page.Home = function (config) {
	config = config || {};
	Ext.applyIf(config, {
		components: [{
			xtype: 'msmotivation-panel-home', renderTo: 'msmotivation-panel-home-div'
		}]
	});
	msMotivation.page.Home.superclass.constructor.call(this, config);
};
Ext.extend(msMotivation.page.Home, MODx.Component);
Ext.reg('msmotivation-page-home', msMotivation.page.Home);