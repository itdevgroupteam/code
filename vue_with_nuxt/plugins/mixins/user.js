import Vue from 'vue'
// import H from '~/utils/helper'

import User from '~/models/User'

const Validation = {
  install(Vue, options) {
    Vue.mixin({
      computed: {
        authenticated() {
          return this.$auth.loggedIn
        },
        user() {
          const user = this.$auth.user ? this.$auth.user : {}
          return new User(user)
        },
      },
    })
  },
}

Vue.use(Validation)
