export default function ({ $axios, store, redirect, error }) {
  $axios.onError((err) => {
    const code = parseInt(err.response && err.response.status)
    const errs = prepareErrors(err.response)

    if (code === 422) {
      store.dispatch('validation/setErrors', errs)
    }

    if (code === 403 || code === 404 || code >= 500) {
      store.dispatch('validation/setErrors', errs)
      error(errs)
    }

    // if (code === 403) {
    //   const errs = {
    //     message: "Forbidden! You can't do this action",
    //     fields: null,
    //     statusCode: 403
    //   }
    //   store.dispatch('validation/setErrors', errs)
    //   error(errs)
    // }
    // return Promise.reject(error)
  })

  $axios.onRequest(() => {
    store.dispatch('validation/clearErrors')
  })

  const prepareErrors = function (response) {
    const fields = {}
    let msg = ''
    response.data.errors.forEach(function (error) {
      if (error.source) {
        fields[error.source.parameter] = error.detail
      } else {
        msg += error.detail
      }
    })
    const errors = {
      message: msg,
      fields,
      statusCode: response.status,
    }
    return errors
  }
}
