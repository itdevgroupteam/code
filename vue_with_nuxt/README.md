# nda | SOME MODULE

> ❯ This project was generated with Node v12.14.0, Yarn v1.21.4 and Nuxt.js v2.12.0. 

[Staging Here](https://nda.com/)

## Usage
If you want to use this project, make sure you've already installed NodeJS and Yarn on your local machine. And then, you can use commands below.

## Installation
### Clone repo

``` bash
# clone the repo
$ git clone git@bitbucket.org:itdevgroupteam/nda.git nda-ui

# go into app's directory
$ cd nda-ui

# install app's dependencies
$ yarn install # Or npm install

# make .env file and set in it necessary values
$ cp .env.example .env
```
### Development

``` bash
# serve with hot reloading at localhost:3000
$ yarn dev # Or npm run dev if you use them
```
Go to [http://localhost:3000](http://localhost:3000)

### Production build

``` bash
# Build for production and launch server
$ yarn build
$ yarn start

# or use docker-compose
$ docker-compose -p nda-ui up -d --build
```
