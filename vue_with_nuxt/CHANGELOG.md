# Changelog
All notable changes to this project will be documented in this file.

## 1.4.4 (01-07-2020)
### Changed
- On a dashboard for Expired task show date if exists, instead of time

## 1.4.3 (26-06-2020)
### Fixed
- On a dashboard show only not closed activities 

## 1.4.2 (26-06-2020)
### Fixed
- On a dashboard show only not closed activities 

## 1.4.1 (23-06-2020)
### Fixed
- Convert lead and convert bid buttons
### Removed
- console.log in Activity table component

## 1.4.0 (23-06-2020)
### Added
- Short activities table for today for current user on dashboard.
- Statuses for activities. Close activity button for changing activity status to Closed.
- Icons for CloseActivityButton and DeleteButton components
### Changed
- Date and time format in tables
- Set default value for time field in reminder form

## 1.3.1 (19-06-2020)
### Fixed
- Create project page

## 1.3.0 (17-06-2020)
### Added
- Component Notes which combine two previous component NoteForm and NoteList
- Lead notes and Lead activities to Deal page and Customer page
- Convert Lead to Customer
- Field address to Lead form

## 1.2.0 (16-06-2020)
### Added
- Lead Activities table on Lead page

## 1.1.2 (16-06-2020)
### Added
- Copy button for lead in activity form

## 1.1.1 (15-06-2020)
### Updated
- For fields link, linkedIn, website, url in forms and for tables appended button which redirect to link

## 1.1.0 (13-06-2020)
### Updated
- DeleteButton as a component for different entities.
- Replaced b-form-select to custom DropdownList component for getting options automatically.
- Adaptive for filters
- Nuxt version to 2.12
- Dependencies and devDependencies. Introduced new rules for prettier.
### Added
- Go Back button as a component
- Projects table with filters and Create/update Project form
- Customers table with filters and Create/update Customer form
- Deals table with filters and Create/update deal form
- Activities table with filters and Create/update activity form with reminder options
- Teams for team lead
- 403 error page

## 1.0.0 (20-02-2020)
### Added
- Login form
- Leads with filters by source and status. CRUD for lead
- Bids with search and filter by source, status, owner. CRUD for bid using popup form
- Bids report with chart an multiply select in filters
- Date rage filter for bids, bids report and leads
- Transfer bid to lead
- Breadcrumbs
