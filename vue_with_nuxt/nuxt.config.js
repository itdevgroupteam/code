require('dotenv').config()

export default {
  mode: 'spa',
  head: {
    htmlAttrs: {
      lang: 'en',
    },
    title: 'IT DEV GROUP CRM',
    meta: [
      { charset: 'utf-8' },
      {
        name: 'viewport',
        content:
          'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no',
      },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || '',
      },
      {
        hid: 'robots',
        name: 'robots',
        content: 'noindex, nofollow',
      },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },
  loading: { color: '#5C99EA' },
  watch: ['~/.env'],
  css: [
    { src: '~/assets/style/global-style/style.scss', lang: 'scss' },
    'quill/dist/quill.snow.css',
    'quill/dist/quill.bubble.css',
    'quill/dist/quill.core.css',
  ],
  router: {
    middleware: ['clearValidationErrors'],
  },
  plugins: [
    '~/plugins/axios',
    '~/plugins/vue2-perfect-scrollbar',
    '~/plugins/vue-clipboard2',
    { src: '~plugins/vue-quill-editor.js', mode: 'client' },
    { src: '~plugins/vue-select.js', mode: 'client' },
    '~/plugins/mixins/validation',
    '~/plugins/mixins/settings',
    '~/plugins/mixins/user',
    '~/plugins/filters/vue2-filters',
    '~/plugins/filters/yesno',
    '~/plugins/filters/currency',
    '~/plugins/directives',
  ],
  auth: {
    strategies: {
      local: {
        endpoints: {
          login: {
            url: 'auth/login',
            method: 'post',
            propertyName: 'data.attributes.accessToken',
          },
          user: {
            url: 'users/me',
            method: 'get',
            propertyName: 'data',
          },
          logout: {
            url: 'auth/logout',
            method: 'get',
          },
        },
      },
    },
    redirect: {
      login: '/',
      logout: '/',
      home: '/dashboard',
    },
    plugins: ['./plugins/auth'],
    cookie: {
      options: {
        sameSite: true,
      },
    },
    localStorage: false,
    resetOnError: true,
  },
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/auth',
    '@nuxtjs/router',
    '@nuxtjs/dotenv',
    '@nuxtjs/proxy',
    'bootstrap-vue/nuxt',
  ],
  buildModules: ['@nuxtjs/dotenv', '@nuxtjs/moment'],
  bootstrapVue: {
    icons: true, // Install the IconsPlugin (in addition to BootStrapVue plugin
  },
  axios: {
    proxy: process.env.AXIOS_PROXY === 'true',
    proxyHeaders: true,
    https: process.env.API_HTTPS === 'true',
    progress: true,
    debug: process.env.AXIOS_DEBUG === 'true',
  },
  proxy: [
    (process.env.API_HTTPS === 'true' ? 'https://' : 'http://') +
      process.env.API_HOST +
      ':' +
      process.env.API_PORT +
      process.env.API_PREFIX,
  ],
  build: {
    extractCSS: true,
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
        })
      }
    },
  },
  vue: {
    config: {
      productionTip: process.env.NODE_ENV === 'production',
      devtools: process.env.NODE_ENV !== 'production',
    },
  },
}
