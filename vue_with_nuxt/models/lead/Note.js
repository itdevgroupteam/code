import H from '~/utils/helper'

export default class Note {
  constructor(obj = {}) {
    if (obj instanceof Note) {
      Object.keys(obj).forEach((element) => {
        this[element] = obj[element]
      })
    } else {
      this.id = !H.isEmpty(obj) && obj.id ? obj.id : null

      this.note =
        !H.isEmpty(obj) && obj.attributes.note ? obj.attributes.note : ''

      this.createdAt =
        !H.isEmpty(obj) && obj.attributes.createdAt
          ? obj.attributes.createdAt
          : ''

      this.updatedAt =
        !H.isEmpty(obj) && obj.attributes.updatedAt
          ? obj.attributes.updatedAt
          : ''
    }
  }

  store() {
    return {
      note: this.note,
    }
  }
}
