import LeadSource from './LeadSource'
import Status from './Status'

import Country from '~/models/Country'
import LeadOwner from '~/models/User'
import Industry from '~/models/Industry'

import H from '~/utils/helper'

export default class Lead {
  constructor(obj = {}) {
    if (obj instanceof Lead) {
      Object.keys(obj).forEach((element) => {
        this[element] = obj[element]
      })
    } else {
      this.id = !H.isEmpty(obj) && obj.id ? obj.id : null
      this.bidId =
        !H.isEmpty(obj) && obj.attributes.bidId ? obj.attributes.bidId : null
      this.customer =
        !H.isEmpty(obj) && obj.attributes.customer
          ? obj.attributes.customer
          : null

      this.title =
        !H.isEmpty(obj) && obj.attributes.title ? obj.attributes.title : ''
      this.email =
        !H.isEmpty(obj) && obj.attributes.email ? obj.attributes.email : ''
      this.firstName =
        !H.isEmpty(obj) && obj.attributes.firstName
          ? obj.attributes.firstName
          : ''
      this.lastName =
        !H.isEmpty(obj) && obj.attributes.lastName
          ? obj.attributes.lastName
          : ''
      this.description =
        !H.isEmpty(obj) && obj.attributes.description
          ? obj.attributes.description
          : ''
      this.rejectionReason =
        !H.isEmpty(obj) && obj.attributes.rejectionReason
          ? obj.attributes.rejectionReason
          : ''

      this.company =
        !H.isEmpty(obj) && obj.attributes.company ? obj.attributes.company : ''

      this.address =
        !H.isEmpty(obj) && obj.attributes.address ? obj.attributes.address : ''

      this.skype =
        !H.isEmpty(obj) && obj.attributes.skype ? obj.attributes.skype : ''

      this.linkedin =
        !H.isEmpty(obj) && obj.attributes.linkedin
          ? H.prepareUrl(obj.attributes.linkedin, true)
          : ''
      this.link =
        !H.isEmpty(obj) && obj.attributes.link
          ? H.prepareUrl(obj.attributes.link, true)
          : ''
      this.phone =
        !H.isEmpty(obj) && obj.attributes.phone ? obj.attributes.phone : ''

      this.website =
        !H.isEmpty(obj) && obj.attributes.website
          ? H.prepareUrl(obj.attributes.website, true)
          : ''
      this.createdAt =
        !H.isEmpty(obj) && obj.attributes.createdAt
          ? obj.attributes.createdAt
          : ''
      this.updatedAt =
        !H.isEmpty(obj) && obj.attributes.updatedAt
          ? obj.attributes.updatedAt
          : ''
      this.country =
        !H.isEmpty(obj, 'relationships') &&
        !H.isEmpty(obj.relationships, 'country') &&
        !H.isEmpty(obj.relationships.country.data)
          ? new Country(obj.relationships.country.data)
          : new Country()

      this.leadSource =
        !H.isEmpty(obj, 'relationships') &&
        !H.isEmpty(obj.relationships, 'leadSource')
          ? new LeadSource(obj.relationships.leadSource.data)
          : new LeadSource()

      this.industry =
        !H.isEmpty(obj, 'relationships') &&
        !H.isEmpty(obj.relationships, 'industry')
          ? new Industry(obj.relationships.industry.data)
          : new Industry()

      this.status =
        !H.isEmpty(obj, 'relationships') &&
        !H.isEmpty(obj.relationships, 'status')
          ? new Status(obj.relationships.status.data)
          : new Status()

      this.leadOwner =
        !H.isEmpty(obj, 'relationships') &&
        !H.isEmpty(obj.relationships, 'owner')
          ? new LeadOwner(obj.relationships.owner.data)
          : new LeadOwner()
    }
  }

  get fullName() {
    return this.firstName.concat(' ', this.lastName)
  }

  store() {
    return {
      owner: this.leadOwner.id,
      leadSource: this.leadSource.id,
      firstName: this.firstName,
      lastName: this.lastName,
      title: this.title,
      email: this.email,
      phone: this.phone,
      company: this.company,
      address: this.address,
      website: this.website,
      skype: this.skype,
      linkedin: this.linkedin,
      link: this.link,
      industry: this.industry.id,
      status: this.status.id,
      description: this.description,
      country: this.country.id,
      rejectionReason: this.rejectionReason,
    }
  }
}
