import H from '~/utils/helper'

export default class LeadSource {
  constructor(obj = {}) {
    if (obj instanceof LeadSource) {
      Object.keys(obj).forEach((element) => {
        this[element] = obj[element]
      })
    } else {
      this.id = !H.isEmpty(obj) && obj.id ? obj.id : null

      this.description =
        !H.isEmpty(obj) && obj.attributes.description
          ? obj.attributes.description
          : ''

      this.title =
        !H.isEmpty(obj) && obj.attributes.name
          ? obj.attributes.name
          : this.description

      this.isActive =
        !H.isEmpty(obj) && obj.attributes.isActive
          ? obj.attributes.isActive
          : false

      this.createdAt =
        !H.isEmpty(obj) && obj.attributes.createdAt
          ? obj.attributes.createdAt
          : ''

      this.updatedAt =
        !H.isEmpty(obj) && obj.attributes.updatedAt
          ? obj.attributes.updatedAt
          : ''
    }
  }
}
