import H from '~/utils/helper'

export default class Status {
  constructor(obj = {}) {
    if (obj instanceof Status) {
      Object.keys(obj).forEach((element) => {
        this[element] = obj[element]
      })
    } else {
      this.id = !H.isEmpty(obj) && obj.id ? obj.id : null

      this.title =
        !H.isEmpty(obj) && obj.attributes.title ? obj.attributes.title : ''
      this.color =
        !H.isEmpty(obj) && obj.attributes.color
          ? obj.attributes.color
          : 'deepskyblue'

      this.rank =
        !H.isEmpty(obj) && obj.attributes.rank ? obj.attributes.rank : 1

      this.createdAt =
        !H.isEmpty(obj) && obj.attributes.createdAt
          ? obj.attributes.createdAt
          : ''

      this.updatedAt =
        !H.isEmpty(obj) && obj.attributes.updatedAt
          ? obj.attributes.updatedAt
          : ''
    }
  }
}
