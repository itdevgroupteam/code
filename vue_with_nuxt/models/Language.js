import H from '~/utils/helper'

export default class Language {
  constructor(obj = {}) {
    if (obj instanceof Language) {
      Object.keys(obj).forEach((element) => {
        this[element] = obj[element]
      })
    } else {
      this.id = !H.isEmpty(obj) && obj.id ? obj.id : null

      this.description =
        !H.isEmpty(obj) && obj.attributes.description
          ? obj.attributes.description
          : ''

      this.title =
        !H.isEmpty(obj) && obj.attributes.title
          ? this.description
            ? `${this.description} (${obj.attributes.title})`
            : obj.attributes.title
          : ''

      this.code =
        !H.isEmpty(obj) && obj.attributes.code ? obj.attributes.code : ''

      this.isDefault =
        !H.isEmpty(obj) && obj.attributes.isDefault
          ? obj.attributes.isDefault
          : false

      this.createdAt =
        !H.isEmpty(obj) && obj.attributes.createdAt
          ? obj.attributes.createdAt
          : ''
      this.updatedAt =
        !H.isEmpty(obj) && obj.attributes.updatedAt
          ? obj.attributes.updatedAt
          : ''
    }
  }
}
