import Status from './Status'
import Type from './Type'
import PaymentType from './PaymentType'
import Source from './Source'

import JobType from '~/models/JobType'
import Industry from '~/models/Industry'
import Technology from '~/models/Technology'
import Country from '~/models/Country'
import Language from '~/models/Language'
import User from '~/models/User'
import Customer from '~/models/customer'

import H from '~/utils/helper'

export default class Project {
  constructor(obj = {}) {
    if (obj instanceof Project) {
      Object.keys(obj).forEach((element) => {
        this[element] = obj[element]
      })
    } else {
      this.id = !H.isEmpty(obj) && obj.id ? obj.id : null
      this.title =
        !H.isEmpty(obj) && obj.attributes.title ? obj.attributes.title : ''

      this.projectKey =
        !H.isEmpty(obj) && obj.attributes.projectKey
          ? obj.attributes.projectKey
          : ''

      this.description =
        !H.isEmpty(obj) && obj.attributes.description
          ? obj.attributes.description
          : ''

      this.rate =
        !H.isEmpty(obj) && obj.attributes.rate ? obj.attributes.rate : ''

      this.underNda =
        !H.isEmpty(obj) && obj.attributes.underNda
          ? +obj.attributes.underNda
          : 0

      this.projectUrl =
        !H.isEmpty(obj) && obj.attributes.projectUrl
          ? H.prepareUrl(obj.attributes.projectUrl, true)
          : ''
      this.nps = !H.isEmpty(obj) && obj.attributes.nps ? +obj.attributes.nps : 1

      this.seo = !H.isEmpty(obj) && +obj.attributes.seo ? obj.attributes.seo : 0

      this.estimatedSum =
        !H.isEmpty(obj) && obj.attributes.estimatedSum
          ? obj.attributes.estimatedSum
          : ''

      this.totalSpendTime =
        !H.isEmpty(obj) && obj.attributes.totalSpendTime
          ? obj.attributes.totalSpendTime
          : ''

      this.customerReview =
        !H.isEmpty(obj) && obj.attributes.customerReview
          ? obj.attributes.customerReview
          : ''

      this.startDate =
        !H.isEmpty(obj) && obj.attributes.startDate
          ? obj.attributes.startDate
          : ''

      this.createdAt =
        !H.isEmpty(obj) && obj.attributes.createdAt
          ? obj.attributes.createdAt
          : ''
      this.updatedAt =
        !H.isEmpty(obj) && obj.attributes.updatedAt
          ? obj.attributes.updatedAt
          : ''

      this.customer =
        !H.isEmpty(obj, 'relationships') &&
        !H.isEmpty(obj.relationships, 'customer')
          ? new Customer(obj.relationships.customer.data)
          : new Customer()

      this.country =
        !H.isEmpty(obj, 'relationships') &&
        !H.isEmpty(obj.relationships, 'country')
          ? new Country(obj.relationships.country.data)
          : new Country()

      this.status =
        !H.isEmpty(obj, 'relationships') &&
        !H.isEmpty(obj.relationships, 'status')
          ? new Status(obj.relationships.status.data)
          : new Status()

      this.projectManager =
        !H.isEmpty(obj, 'relationships') &&
        !H.isEmpty(obj.relationships, 'projectManager')
          ? new User(obj.relationships.projectManager.data)
          : new User()

      this.leadgen =
        !H.isEmpty(obj, 'relationships') &&
        !H.isEmpty(obj.relationships, 'leadgen')
          ? new User(obj.relationships.leadgen.data)
          : new User()

      this.paymentType =
        !H.isEmpty(obj, 'relationships') &&
        !H.isEmpty(obj.relationships, 'paymentType')
          ? new PaymentType(obj.relationships.paymentType.data)
          : new PaymentType()

      this.projectType =
        !H.isEmpty(obj, 'relationships') &&
        !H.isEmpty(obj.relationships, 'projectType')
          ? new Type(obj.relationships.projectType.data)
          : new Type()

      this.jobType =
        !H.isEmpty(obj, 'relationships') &&
        !H.isEmpty(obj.relationships, 'jobType')
          ? new JobType(obj.relationships.jobType.data)
          : new JobType()

      this.projectSource =
        !H.isEmpty(obj, 'relationships') &&
        !H.isEmpty(obj.relationships, 'projectSource')
          ? new Source(obj.relationships.projectSource.data)
          : new Source()

      this.industries =
        !H.isEmpty(obj, 'relationships') &&
        !H.isEmpty(obj.relationships, 'industries') &&
        Array.isArray(obj.relationships.industries.data) &&
        obj.relationships.industries.data.length
          ? obj.relationships.industries.data.map((o) => new Industry(o))
          : []

      this.technologies =
        !H.isEmpty(obj, 'relationships') &&
        !H.isEmpty(obj.relationships, 'technologies') &&
        Array.isArray(obj.relationships.technologies.data) &&
        obj.relationships.technologies.data.length
          ? obj.relationships.technologies.data.map((o) => new Technology(o))
          : []

      this.languages =
        !H.isEmpty(obj, 'relationships') &&
        !H.isEmpty(obj.relationships, 'languages') &&
        Array.isArray(obj.relationships.languages.data) &&
        obj.relationships.languages.data.length
          ? obj.relationships.languages.data.map((o) => new Language(o))
          : []
    }
  }

  store() {
    return {
      title: this.title,
      description: this.description,
      projectKey: this.projectKey,
      rate: this.rate,
      underNda: this.underNda,
      projectUrl: this.projectUrl,
      nps: this.nps,
      seo: this.seo,
      estimatedSum: this.estimatedSum,
      totalSpendTime: this.totalSpendTime,
      customerReview: this.customerReview,
      startDate: this.startDate,
      status: this.status.id,
      projectManager: this.projectManager.id,
      leadgen: this.leadgen.id,
      customer: this.customer.id,
      country: this.country.id,
      paymentType: this.paymentType.id,
      projectType: this.projectType.id,
      jobType: this.jobType.id,
      projectSource: this.projectSource.id,
      industries: this.industries.map((e) => e.id),
      languages: this.languages.map((e) => e.id),
      technologies: this.technologies.map((e) => e.id),
    }
  }
}
