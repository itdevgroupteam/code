import H from '~/utils/helper'

export default class Type {
  constructor(obj = {}) {
    if (obj instanceof Type) {
      Object.keys(obj).forEach((element) => {
        this[element] = obj[element]
      })
    } else {
      this.id = !H.isEmpty(obj) && obj.id ? obj.id : null

      this.title =
        !H.isEmpty(obj) && obj.attributes.title ? obj.attributes.title : ''

      this.createdAt =
        !H.isEmpty(obj) && obj.attributes.createdAt
          ? obj.attributes.createdAt
          : ''
      this.updatedAt =
        !H.isEmpty(obj) && obj.attributes.updatedAt
          ? obj.attributes.updatedAt
          : ''
    }
  }
}
