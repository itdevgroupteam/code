import H from '~/utils/helper'

export default class Status {
  constructor(obj = {}) {
    if (obj instanceof Status) {
      Object.keys(obj).forEach((element) => {
        this[element] = obj[element]
      })
    } else {
      this.id = !H.isEmpty(obj) && obj.id ? obj.id : null

      this.title =
        !H.isEmpty(obj) && obj.attributes.title ? obj.attributes.title : ''

      this.description =
        !H.isEmpty(obj) && obj.attributes.description
          ? obj.attributes.description
          : ''

      this.createdAt =
        !H.isEmpty(obj) && obj.attributes.createdAt
          ? obj.attributes.createdAt
          : ''

      this.updatedAt =
        !H.isEmpty(obj) && obj.attributes.updatedAt
          ? obj.attributes.updatedAt
          : ''
    }
  }
}
