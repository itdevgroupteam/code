import Lead from '~/models/lead'
import LeadSource from '~/models/lead/LeadSource'
import Status from '~/models/lead/Status'

import H from '~/utils/helper'

export const actions = {
  async create({ commit }, formData) {
    const params = H.appendFormData(formData)
    try {
      const { data } = await this.$axios.$post('leads', params, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      })
      return new Lead(data)
    } catch (e) {
      throw e
    }
  },
  async delete({ commit }, id) {
    try {
      await this.$axios.delete(`leads/${id}`)
    } catch (e) {
      throw e
    }
  },
  async update({ commit }, params = {}) {
    try {
      const { data } = await this.$axios.$put(`leads/${params.id}`, params)
      return new Lead(data)
    } catch (e) {
      throw e
    }
  },
  async fetchList({ commit }, params = {}) {
    try {
      const { data, meta } = await this.$axios.$get('leads', { params })
      const leads = data.map((o) => new Lead(o))
      return { data: leads, meta }
    } catch (e) {
      throw e
    }
  },
  async fetchById({ commit }, id) {
    try {
      const { data } = await this.$axios.$get(`leads/${id}`)
      return new Lead(data)
    } catch (e) {
      throw e
    }
  },
  async fetchStatusList({ commit }, params = {}) {
    try {
      const { data } = await this.$axios.$get('leads/statuses', {
        params,
      })
      commit('SET_STATUS_LIST', data)
    } catch (e) {
      throw e
    }
  },
  async fetchSourceList({ commit }, params = {}) {
    try {
      const { data } = await this.$axios.$get('leads/sources', {
        params,
      })
      commit('SET_SOURCE_LIST', data)
    } catch (e) {
      throw e
    }
  },
  async convert({ commit }, { id, form }) {
    const params = H.appendFormData(form)
    try {
      const { data } = await this.$axios.$post(`leads/${id}/convert`, params)
      return new Lead(data)
    } catch (e) {
      throw e
    }
  },
}

export const mutations = {
  SET_STATUS_LIST(state, statusList) {
    state.statusList = statusList
  },
  SET_SOURCE_LIST(state, sourceList) {
    state.sourceList = sourceList
  },
}
export const getters = {
  statusList: (state) =>
    state.statusList.length
      ? state.statusList.map((a) => {
          const o = new Status(a)
          return { value: o.id, text: o.title }
        })
      : state.statusList,

  sourceList: (state) =>
    state.sourceList.length
      ? state.sourceList.map((a) => {
          const o = new LeadSource(a)
          return { value: o.id, text: o.title }
        })
      : state.sourceList,
}

export const state = () => ({
  statusList: [],
  sourceList: [],
})
