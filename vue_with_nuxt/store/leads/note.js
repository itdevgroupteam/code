import Note from '~/models/lead/Note'

import H from '~/utils/helper'

export const actions = {
  async create({ commit }, { leadId, formData }) {
    const params = H.appendFormData(formData)
    try {
      const { data } = await this.$axios.$post(
        `leads/${leadId}/notes`,
        params,
        {
          headers: {
            'Content-Type': 'multipart/form-data',
          },
        }
      )
      return new Note(data)
    } catch (e) {
      throw e
    }
  },
  async delete({ commit }, { leadId, noteId }) {
    try {
      await this.$axios.delete(`leads/${leadId}/notes/${noteId}`)
    } catch (e) {
      throw e
    }
  },
  async update({ commit }, { leadId, noteId, note }) {
    try {
      const { data } = await this.$axios.$put(
        `leads/${leadId}/notes/${noteId}`,
        { note }
      )
      return new Note(data)
    } catch (e) {
      throw e
    }
  },
  async fetchList({ commit }, { leadId, params = {} }) {
    const defaultParams = {
      page: 1,
      perPage: 10,
    }
    try {
      const { data, meta } = await this.$axios.$get(`leads/${leadId}/notes`, {
        params: { ...defaultParams, ...params },
      })
      const notes = data.map((o) => new Note(o))
      return { data: notes, meta }
    } catch (e) {
      throw e
    }
  },
  async fetchById({ commit }, { leadId, noteId }) {
    try {
      const { data } = await this.$axios.$get(`leads/${leadId}/notes/${noteId}`)
      return new Note(data)
    } catch (e) {
      throw e
    }
  },
}
