import Settings from '~/models/Settings'
import Country from '~/models/Country'
import Industry from '~/models/Industry'
import Language from '~/models/Language'
import Technology from '~/models/Technology'
import JobType from '~/models/JobType'
import User from '~/models/User'

export const state = () => ({
  settings: false,
  countries: [],
  industries: [],
  languages: [],
  technologies: [],
  jobTypes: [],
  projectManagers: [],
  sales: [],
})

export const getters = {
  settings(state) {
    return state.settings.length ? new Settings(state.settings) : new Settings()
  },
  countries: (state) =>
    state.countries.length
      ? state.countries.map((e) => ({ value: e.id, text: e.title }))
      : state.countries,
  industries: (state) =>
    state.industries.length
      ? state.industries.map((a) => {
          const o = new Industry(a)
          return { value: o.id, text: o.title }
        })
      : state.industries,
  languages: (state) =>
    state.languages.length
      ? state.languages.map((a) => {
          const o = new Language(a)
          return { value: o.id, text: o.title }
        })
      : state.languages,
  technologies: (state) =>
    state.technologies.length
      ? state.technologies.map((a) => {
          const o = new Technology(a)
          return { value: o.id, text: o.title }
        })
      : state.technologies,
  jobTypes: (state) =>
    state.jobTypes.length
      ? state.jobTypes.map((a) => {
          const o = new JobType(a)
          return { value: o.id, text: o.title }
        })
      : state.jobTypes,
  projectManagers: (state) =>
    state.projectManagers.length
      ? state.projectManagers.map((a) => {
          const o = new User(a)
          return { value: o.id, text: o.name }
        })
      : state.projectManagers,
  sales: (state) =>
    state.sales.length
      ? state.sales.map((a) => {
          const o = new User(a)
          return { value: o.id, text: o.name }
        })
      : state.sales,
}

export const mutations = {
  SET_SETTINGS(state, settings) {
    state.settings = settings
  },
  SET_COUNTRIES(state, countries) {
    state.countries = countries
  },
  SET_INDUSTRIES(state, industries) {
    state.industries = industries
  },
  SET_LANGUAGES(state, languages) {
    state.languages = languages
  },
  SET_TECHNOLOGIES(state, technologies) {
    state.technologies = technologies
  },
  SET_JOB_TYPES(state, jobTypes) {
    state.jobTypes = jobTypes
  },
  SET_PROJECT_MANAGERS(state, projectManagers) {
    state.projectManagers = projectManagers
  },
  SET_SALES(state, sales) {
    state.sales = sales
  },
}

export const actions = {
  // async nuxtServerInit({ dispatch, state }) {
  //   if (!state.settings) {
  //     await dispatch('getSettings')
  //   }
  // },
  async getSettings({ commit }) {
    try {
      const { data } = await this.$axios.$get('settings')
      commit('SET_SETTINGS', data)
    } catch (e) {}
  },

  async fetchCountries({ commit }, formData = {}) {
    const params = Object.assign(
      {},
      { page: 1, perPage: 100, sort: 'description' },
      formData
    )
    try {
      const { data } = await this.$axios.$get('countries', {
        params,
      })
      const countries = data
        .map((o) => new Country(o))
        .sort((a, b) => {
          if (a.title < b.title) {
            return -1
          }
          if (a.title > b.title) {
            return 1
          }
          return 0
        })
      commit('SET_COUNTRIES', countries)
      return countries
    } catch (e) {}
  },
  async fetchIndustries({ commit }, params = {}) {
    try {
      const { data } = await this.$axios.$get('industries', {
        params,
      })
      commit('SET_INDUSTRIES', data)
    } catch (e) {
      throw e
    }
  },
  async fetchLanguages({ commit }, params = {}) {
    try {
      const { data } = await this.$axios.$get('languages', {
        params,
      })
      const languages = data
        .map((o) => new Language(o))
        .sort((a, b) => {
          if (a.title < b.title) {
            return -1
          }
          if (a.title > b.title) {
            return 1
          }
          return 0
        })
      commit('SET_LANGUAGES', languages)
    } catch (e) {
      throw e
    }
  },
  async fetchTechnologies({ commit }, params = {}) {
    try {
      const { data } = await this.$axios.$get('technologies', {
        params,
      })
      commit('SET_TECHNOLOGIES', data)
    } catch (e) {
      throw e
    }
  },
  async fetchJobTypes({ commit }, params = {}) {
    try {
      const { data } = await this.$axios.$get('job-types', {
        params,
      })
      commit('SET_JOB_TYPES', data)
    } catch (e) {
      throw e
    }
  },
  async fetchProjectManagers({ commit }, formData = {}) {
    const params = Object.assign(
      {},
      { page: 1, perPage: 100, sort: 'name' },
      formData
    )
    try {
      const { data } = await this.$axios.$get('users/pms', {
        params,
      })
      commit('SET_PROJECT_MANAGERS', data)
    } catch (e) {
      throw e
    }
  },
  async fetchSales({ commit }, formData = {}) {
    const params = Object.assign(
      {},
      { page: 1, perPage: 100, sort: 'name' },
      formData
    )
    try {
      const { data } = await this.$axios.$get('users/sales', {
        params,
      })
      commit('SET_SALES', data)
    } catch (e) {
      throw e
    }
  },
}
