import Project from '~/models/project'
import Status from '~/models/project/Status'
import Source from '~/models/project/Source'
import Type from '~/models/project/Type'
import PaymentType from '~/models/project/PaymentType'

// import H from '~/utils/helper'

const END_POINT = 'projects'

export const actions = {
  async create({ commit }, params = {}) {
    // const params = H.appendFormData(formData)
    try {
      const { data } = await this.$axios.$post(`${END_POINT}`, params)
      return new Project(data)
    } catch (e) {
      throw e
    }
  },
  async delete({ commit }, id) {
    try {
      await this.$axios.delete(`${END_POINT}/${id}`)
    } catch (e) {
      throw e
    }
  },
  async update({ commit }, params = {}) {
    try {
      const { data } = await this.$axios.$put(
        `${END_POINT}/${params.id}`,
        params
      )
      return new Project(data)
    } catch (e) {
      throw e
    }
  },
  async fetchList({ commit }, params = {}) {
    try {
      const { data, meta } = await this.$axios.$get(`${END_POINT}`, { params })
      const projects = data.map((o) => new Project(o))
      return { data: projects, meta }
    } catch (e) {
      throw e
    }
  },
  async fetchById({ commit }, id) {
    try {
      const { data } = await this.$axios.$get(`${END_POINT}/${id}`)
      return new Project(data)
    } catch (e) {
      throw e
    }
  },
  async fetchStatusList({ commit }, params = {}) {
    try {
      const { data } = await this.$axios.$get(`${END_POINT}/statuses`, {
        params,
      })
      commit('SET_STATUS_LIST', data)
    } catch (e) {
      throw e
    }
  },
  async fetchSourceList({ commit }, params = {}) {
    try {
      const { data } = await this.$axios.$get(`${END_POINT}/sources`, {
        params,
      })
      commit('SET_SOURCE_LIST', data)
    } catch (e) {
      throw e
    }
  },
  async fetchTypeList({ commit }, params = {}) {
    try {
      const { data } = await this.$axios.$get(`${END_POINT}/types`, {
        params,
      })
      commit('SET_TYPE_LIST', data)
    } catch (e) {
      throw e
    }
  },
  async fetchPaymentTypeList({ commit }, params = {}) {
    try {
      const { data } = await this.$axios.$get(`${END_POINT}/payment-types`, {
        params,
      })
      commit('SET_PAYMENT_TYPE_LIST', data)
    } catch (e) {
      throw e
    }
  },
}

export const mutations = {
  SET_STATUS_LIST(state, statusList) {
    state.statusList = statusList
  },
  SET_SOURCE_LIST(state, sourceList) {
    state.sourceList = sourceList
  },
  SET_TYPE_LIST(state, typeList) {
    state.typeList = typeList
  },
  SET_PAYMENT_TYPE_LIST(state, paymentTypeList) {
    state.paymentTypeList = paymentTypeList
  },
}
export const getters = {
  statusList: (state) =>
    state.statusList.length
      ? state.statusList.map((a) => {
          const o = new Status(a)
          return { value: o.id, text: o.title }
        })
      : state.statusList,

  sourceList: (state) =>
    state.sourceList.length
      ? state.sourceList.map((a) => {
          const o = new Source(a)
          return { value: o.id, text: o.title }
        })
      : state.sourceList,

  typeList: (state) =>
    state.typeList.length
      ? state.typeList.map((a) => {
          const o = new Type(a)
          return { value: o.id, text: o.title }
        })
      : state.typeList,

  paymentTypeList: (state) =>
    state.paymentTypeList.length
      ? state.paymentTypeList.map((a) => {
          const o = new PaymentType(a)
          return { value: o.id, text: o.title }
        })
      : state.paymentTypeList,
}

export const state = () => ({
  statusList: [],
  sourceList: [],
  typeList: [],
  paymentTypeList: [],
})
