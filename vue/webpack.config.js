const path = require('path')
const webpack = require('webpack')
const Dotenv = require('dotenv-webpack')
const VueLoaderPlugin = require('vue-loader/lib/plugin')

module.exports = {
  mode: process.env.NODE_ENV === 'production' ? 'production' : 'development',
  optimization: {
    minimize: true,
  },
  entry: './src/main.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'dumbo-client-chat.js',
  },
  resolve: {
    extensions: ['.js', '.vue'],
    alias: {
      vue$: 'vue/dist/vue.common.js',
      '@': path.resolve('src'),
    },
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        use: 'vue-loader',
      },
      {
        test: /\.scss$/,
        use: ['vue-style-loader', 'css-loader', 'sass-loader'],
      },
      {
        enforce: 'pre',
        test: /\.(js|vue)$/,
        loader: 'eslint-loader',
        exclude: [
          '/dist',
          '/webpack.config.js',
          '/.eslintrc.js',
          '/.prettierrc',
        ],
      },
    ],
  },
  plugins: [
    new Dotenv({
      path: './.env', // Path to .env file (this is the default)
    }),
    new VueLoaderPlugin(),
    new webpack.IgnorePlugin(/^\.\/locale$/),
  ],
  resolveLoader: {
    alias: {
      'scss-loader': 'sass-loader',
    },
  },
  devServer: {
    hot: true,
    contentBase: [path.join(__dirname, 'dist'), path.join(__dirname, 'demo')],
    compress: true,
    port: 8000,
    writeToDisk: true,
    filename: 'dumbo-client-chat.js',
  },
}

if (process.env.NODE_ENV === 'production') {
  module.exports.devtool = '#source-map'
  // http://vue-loader.vuejs.org/en/workflow/production.html
  module.exports.plugins = (module.exports.plugins || []).concat([
    new webpack.LoaderOptionsPlugin({
      minimize: true,
    }),
  ])
}
