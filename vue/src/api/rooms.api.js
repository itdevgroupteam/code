import httpClient from './httpClient'

const END_POINT = '/support/client/chat/rooms'

const createChatRoom = () => httpClient.post(END_POINT)

const closeChatRoom = (token) => httpClient.put(`${END_POINT}/${token}/close`)

export { createChatRoom, closeChatRoom }
