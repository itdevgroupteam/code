# Dumbo Client Chat Plugin

Vue chat plugin for Dumbo project **can be used in any HTML/JS application.**

```html
<html>
  <body>

    ...

    <dumbo-client-chat/>

    <script src="dumbo-client-chat.js"></script>

  </body>
</html>
```

## How it works

Components are registered in `src/main.js`.

Component code is in `src/components/DumboClientChat.vue`.

To add a component :

- Add a `.vue` file in `src/components`
- Register the component in `src/main.js`

## Development
- create `.env` file based on `.env.example` file and replace `API_BASE_URL` value with your API reference
- Install dependencies : `npm install`
- Launch webpack : `npm run dev` (watch mode)
- Visit ``http://localhost:8000/`` in a browser
- Edit `src/main.js` or your component file
- Refresh the page

## Bundle a release

```
npm run build
```

File will be placed in `dist/dumbo-client-chat.js`.

Refer to `webpack.config.js` for customization options.

## Production usage

To use script on your website use this code:
```html
    <dumbo-client-chat />
    <script type="text/javascript">
      var sc = document.createElement('script');
      sc.type = 'text/javascript';
      sc.src = '[source_url].js?v=' + Date.now();
      document.body.appendChild(sc);
    </script>
```
You should change `[source_url]` to your own link to file