## [Unreleased]
### Update
### Added
- bitbucket-pipline config for deployment to s3
- added custom web components builder
- installed node-sass & sass-loader
- created dumbo-client-chat component
- updated all libraries
- installed axios
- added dotenv
- created .env file
- added models for messages and user
- added vuex
- added chat actions to vuex
- created chat functionality
- added .env to .gitignore
- added Message component
- added store modules
- added axios interceptop
- added api requests file
- added eslint
- added prettier
- added errors snackbar
- installed vue-echo-laravel module
- added websockets chat functionality
- added SessionClosedModal component
### Changed
### Deprecated
### Removed
### Fixed
### Security