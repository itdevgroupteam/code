import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';

import { Observable, throwError } from 'rxjs';
import { catchError, take } from 'rxjs/operators';

import { AuthService } from '../auth.service';
import { SignIn } from '../../shared/models/auth.models';

@Injectable()
export class ApplyTokenInterceptor implements HttpInterceptor {
  constructor(
    private auth: AuthService,
    private jwtHelper: JwtHelperService
  ) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this.auth.isAuthenticated() && !this.jwtHelper.isTokenExpired(this.auth.token())) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${this.auth.token()}`
        }
      });
    } else if (this.auth.token() && this.jwtHelper.isTokenExpired(this.auth.token())) {
      this.auth.auth$.pipe(take(1)).subscribe((auth: SignIn) => {
        this.auth.refreshToken(auth.data.attributes.refreshToken).subscribe((res: SignIn) => {
          this.auth.setToken(res.data.attributes.accessToken);
          request = request.clone({
            setHeaders: {
              Authorization: `Bearer ${this.auth.token()}`
            }
          });
        });
      });
    }

    return next.handle(request).pipe(
      catchError((error: HttpErrorResponse) => {
        if (this.auth.isAuthenticated() && (request.method === 'GET') &&
          (error.status === 500 || 503 || 504 || 403 || 400 || 405 || 401)) {
          this.auth.logout();
        }
        return throwError(error);
      })
    );
  }
}
