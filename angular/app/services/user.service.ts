import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, ReplaySubject } from 'rxjs';
import { tap } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { User } from '../shared/models/user.models';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private url = `${environment.apiUrl}`;
  private userSubj: ReplaySubject<User> = new ReplaySubject<User>(1);
  user$: Observable<User> = this.userSubj.asObservable();

  constructor(
    private http: HttpClient
  ) {
  }

  getUser(): Observable<User> {
    return this.http.get<User>(`${this.url}/users/me`).pipe(tap(user => this.userSubj.next(user)));
  }
}
