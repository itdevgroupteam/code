import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { Article, CreatePayload, FiltersStudies, Studies, StudiesItem } from '../shared/models/studies.models';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StudiesService {
  private url = `${environment.apiUrl}`;
  private studiesSubj: any = new BehaviorSubject(null);
  studies$: Observable<any> = this.studiesSubj.asObservable();

  constructor(
    private http: HttpClient
  ) {
  }

  getStudies(filters: FiltersStudies, isNext?: boolean): Observable<Studies> {
    let params = new HttpParams();
    params = params.append('page[number]', filters.page);
    params = params.append('page[size]', filters.size);
    params = params.append('sort', filters.sort);
    return this.http.get<Studies>(`${this.url}/case-studies`, {params})
      .pipe(
        tap((res: any) => {
          const data: any = isNext ? {data: [...this.studiesSubj.value.data, ...res.data], meta: res.meta} : res;
          this.studiesSubj.next(data);
        }),
        map((res: Studies) => {
          return {
            ...res,
            data: res.data.map(item => {
              return {...item, isCopied: false};
            })
          };
        })
      );
  }

  resetStudies(): void {
    this.studiesSubj.next(null);
  }

  createStudy(payload: CreatePayload): Observable<StudiesItem> {
    return this.http.post<StudiesItem>(`${this.url}/case-studies`, payload);
  }

  editStudy(id: number, payload: CreatePayload): Observable<Article> {
    return this.http.put<Article>(`${this.url}/case-studies/${id}`, payload);
  }

  getArticleBySlug(slug: string): Observable<Article> {
    return this.http.get<Article>(`${this.url}/case-studies/slug/${slug}`);
  }

  getArticleById(id: number): Observable<Article> {
    return this.http.get<Article>(`${this.url}/case-studies/${id}`);
  }
}
