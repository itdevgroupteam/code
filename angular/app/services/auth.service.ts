import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Params, Router } from '@angular/router';

import { Observable, ReplaySubject } from 'rxjs';
import { tap } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { PasswordRecoveryPayload, SignIn, SignUpActivationPayload, SignUpPayload } from '../shared/models/auth.models';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private url = `${environment.apiUrl}/auth`;

  private authSubj: ReplaySubject<SignIn> = new ReplaySubject<SignIn>(1);
  auth$: Observable<SignIn> = this.authSubj.asObservable();

  constructor(
    private http: HttpClient,
    private router: Router
  ) {
  }

  token(): string {
    return localStorage.getItem('token') || '';
  }

  isAuthenticated(): boolean {
    return !!this.token();
  }

  setToken(token: string): void {
    localStorage.setItem('token', token);
  }

  logout(): void {
    localStorage.removeItem('token');
    this.router.navigate(['/sign-in']);
  }

  signUp(payload: SignUpPayload): Observable<any> {
    return this.http.post<any>(`${this.url}/signup`, payload);
  }

  signUpActivation(payload: Params | SignUpActivationPayload): Observable<any> {
    return this.http.post<any>(`${this.url}/signup/activation`, payload);
  }

  signIn(payload: SignUpPayload): Observable<SignIn> {
    return this.http.post<SignIn>(`${this.url}/login`, payload).pipe(tap((res: SignIn) => this.authSubj.next(res)));
  }

  refreshToken(refreshToken: string): Observable<SignIn> {
    return this.http.put<SignIn>(`${this.url}/token`, {refreshToken});
  }

  passwordRecovery(payload: SignUpPayload): Observable<SignIn> {
    return this.http.post<SignIn>(`${this.url}/password-recovery/get-code`, payload);
  }

  changePassword(payload: PasswordRecoveryPayload): Observable<any> {
    return this.http.put<any>(`${this.url}/password-recovery/change-password`, payload);
  }
}
