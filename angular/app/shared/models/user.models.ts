export interface User {
  data: {
    attributes: {
      email: string;
      firstName: string;
      lastName: string;
    };
    id: number;
    type: string;
  };
}
