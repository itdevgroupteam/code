export interface SignUpPayload {
  email: string;
  password: string;
  passwordConfirm?: string;
  firstName?: string;
  lastName?: string;
}

export interface SignUpActivationPayload {
  code: string;
  email: string;
}

export interface SignUpError {
  firstName?: string | null;
  lastName?: string | null;
  email?: string | null;
  password?: string | null;
  passwordConfirm?: string | null;
}

export interface PasswordRecoveryPayload {
  code: string;
  email: string;
  password: string;
}

export interface SignIn {
  data: {
    attributes: {
      accessToken: string;
      expiresIn: number;
      refreshToken: string;
      tokenType: string;
    };
    type: string;
  };
}

export interface SignInError {
  email?: string | null;
  password?: string | null;
  other?: string | null;
}

export interface PasswordRecoveryError {
  email: string | null;
}

export interface Error {
  detail: string | null;
  source: {
    parameter: string;
  };
  status: number;
}
