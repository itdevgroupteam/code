export type StarterPageType = 'sign-in' | 'sign-up' | null;

export type StarterPageRoute = '/sign-in' | '/sign-up' | null;
