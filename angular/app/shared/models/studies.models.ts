import { SafeHtml } from '@angular/platform-browser';
import { User } from './user.models';

interface StudyAttributes {
  companyBackground: string;
  companyName: string;
  createdAt: string;
  problem: string;
  result: string;
  slug: string;
  solution: string;
  title: string;
  updatedAt: string;
}

interface StudiesItemData {
  attributes: StudyAttributes;
}

export interface Relationships {
  user: User;
}

export interface StudiesItem {
  data: StudiesItemData[];
}

export interface Studies {
  data: StudyItem[];
  meta: {
    totalItems: number;
    totalPages: number;
  };
}

export interface FiltersStudies {
  page: string;
  size: string;
  sort: string;
}

export interface CreatePayload {
  title: string | SafeHtml;
  companyName: string | SafeHtml;
  companyBackground: string | SafeHtml;
  problem: string | SafeHtml;
  solution: string | SafeHtml;
  result: string | SafeHtml;
}

export interface StudyItem {
  attributes: StudyAttributes;
  id: number;
  relationships: Relationships;
  type: string;
  isCopied?: boolean;
}

export interface CreateStudyErrors {
  title: string | null;
  companyName: string | null;
  companyBackground: string | null;
  problem: string | null;
  solution: string | null;
  result: string | null;
}

export interface Article {
  data: {
    attributes: {
      companyBackground: string;
      companyName: string;
      createdAt: string;
      problem: string;
      result: string;
      slug: string;
      solution: string;
      title: string;
      updatedAt: string;
    };
    id: number;
    relationships: Relationships;
    type: string;
  };
}
