import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { EMPTY, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { StudiesService } from '../services/studies.service';
import { Article } from '../shared/models/studies.models';

@Injectable()
export class ArticleResolver implements Resolve<any> {
  constructor(
    private api: StudiesService
  ) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Article | Observable<Article> {
    return this.api.getArticleBySlug(route.params.slug).pipe(
      catchError(() => {
        return EMPTY;
      })
    );
  }
}
