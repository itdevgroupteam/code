import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { EMPTY, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { UserService } from '../services/user.service';
import { User } from '../shared/models/user.models';

@Injectable()
export class UserResolver implements Resolve<any> {
  constructor(
    private api: UserService
  ) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): User | Observable<User> {
    return this.api.getUser().pipe(
      catchError(() => {
        return EMPTY;
      })
    );
  }
}
