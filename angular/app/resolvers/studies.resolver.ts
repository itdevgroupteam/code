import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { EMPTY, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { StudiesService } from '../services/studies.service';
import { FiltersStudies, Studies } from '../shared/models/studies.models';

@Injectable()
export class StudiesResolver implements Resolve<Studies> {
  constructor(
    private api: StudiesService
  ) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Studies | Observable<Studies> {
    const filters: FiltersStudies = {
      sort: '-createdAt',
      page: '1',
      size: '10'
    };
    return this.api.getStudies(filters).pipe(
      catchError(() => {
        return EMPTY;
      })
    );
  }
}
