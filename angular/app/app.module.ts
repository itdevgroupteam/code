import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { JwtModule } from '@auth0/angular-jwt';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';

import { AppRoutingModule } from './app-routing.module';
import { ListingModule } from './components/listing/listing.module';
import { ApplyTokenInterceptor } from './services/interceptors/apply.token.interceptor';
import { AuthService } from './services/auth.service';

import { SignUpComponent } from './components/starter/sign-up/sign-up.component';
import { StarterHeaderComponent } from './components/starter/starter-header/starter-header.component';
import { AppComponent } from './app.component';
import { SignInComponent } from './components/starter/sign-in/sign-in.component';
import { PasswordRecoveryComponent } from './components/starter/password-recovery/password-recovery.component';
import { NewPasswordComponent } from './components/starter/new-password/new-password.component';

export function tokenGetter(): string | null {
  return localStorage.getItem('token');
}

@NgModule({
  declarations: [
    AppComponent,
    SignUpComponent,
    StarterHeaderComponent,
    SignInComponent,
    PasswordRecoveryComponent,
    NewPasswordComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    ListingModule,
    ReactiveFormsModule,
    JwtModule.forRoot({
      config: {
      },
    }),
    AngularEditorModule,
    ScrollToModule.forRoot()
  ],
  providers: [
    AuthService,
    HttpClient,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApplyTokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
