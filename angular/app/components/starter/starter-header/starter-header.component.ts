import { Component, Input } from '@angular/core';
import { StarterPageRoute, StarterPageType } from '../../../shared/models/starter.models';

@Component({
  selector: 'app-starter-header',
  templateUrl: './starter-header.component.html',
  styleUrls: ['./starter-header.component.scss']
})
export class StarterHeaderComponent {
  @Input() page: StarterPageType = null;
  @Input() route: StarterPageRoute = null;
}

