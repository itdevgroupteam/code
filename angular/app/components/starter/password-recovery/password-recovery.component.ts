import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AuthService } from '../../../services/auth.service';
import { StarterPageRoute, StarterPageType } from '../../../shared/models/starter.models';
import { Error, PasswordRecoveryError } from '../../../shared/models/auth.models';

interface Options {
  isSend: boolean;
}

@Component({
  selector: 'app-password-recovery',
  templateUrl: './password-recovery.component.html',
  styleUrls: ['./password-recovery.component.scss']
})
export class PasswordRecoveryComponent implements OnInit {
  page: StarterPageType = null;
  route: StarterPageRoute = null;
  form: FormGroup;
  options: Options = {
    isSend: false
  };
  error: PasswordRecoveryError = {
    email: null
  };

  constructor(
    private fb: FormBuilder,
    private auth: AuthService
  ) {
  }

  ngOnInit(): void {
    this.initForm();
  }

  private initForm(): void {
    this.form = this.fb.group({
      email: ['', [Validators.required, Validators.pattern('[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+')]]
    });
  }

  private resetErrors(): void {
    this.error = {
      email: null
    };
  }

  send(): void {
    this.auth.passwordRecovery(this.form.value).subscribe(() => {
      this.options.isSend = true;
    }, err => {
      this.resetErrors();
      if (err.error.errors.length > 1) {
        err.error.errors.forEach((e: Error) => {
          switch (e.source && e.source.parameter) {
            case 'email':
              this.error.email = e.detail;
              break;
            default:
              console.error(err);
          }
        });
      } else {
        this.error.email = err.error.errors[0].detail;
      }
    });
  }
}
