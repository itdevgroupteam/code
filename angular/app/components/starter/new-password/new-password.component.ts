import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';

import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { StarterPageRoute, StarterPageType } from '../../../shared/models/starter.models';
import { PasswordRecoveryPayload, SignUpActivationPayload } from '../../../shared/models/auth.models';
import { AuthService } from '../../../services/auth.service';

interface Options {
  isCreate: boolean;
}

interface Errors {
  password: string | null;
}

@Component({
  selector: 'app-new-password',
  templateUrl: './new-password.component.html',
  styleUrls: ['./new-password.component.scss']
})
export class NewPasswordComponent implements OnInit, OnDestroy {
  private destroy$: ReplaySubject<void> = new ReplaySubject(1);
  page: StarterPageType = null;
  route: StarterPageRoute = null;
  form: FormGroup;
  options: Options = {
    isCreate: true
  };
  errors: Errors = {
    password: null
  };
  params: Params | SignUpActivationPayload | null = null;

  constructor(
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private auth: AuthService
  ) {
  }

  ngOnInit(): void {
    this.initForm();
    this.subscribeToQueryParams();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  private initForm(): void {
    this.form = this.fb.group({
      password: ['', [Validators.required, Validators.minLength(8)]]
    });
  }

  private subscribeToQueryParams(): void {
    this.activatedRoute.queryParams
      .pipe(takeUntil(this.destroy$))
      .subscribe((params: Params | SignUpActivationPayload) => {
        if (Object.keys(params).length > 0) {
          this.params = params;
        } else {
          this.params = null;
        }
      });
  }

  create(): void {
    const payload: PasswordRecoveryPayload = {
      ...this.params,
      ...this.form.value
    };
    this.auth.changePassword(payload).subscribe(() => {
      this.options.isCreate = false;
    }, err => {
      console.error(err);
    });
  }
}
