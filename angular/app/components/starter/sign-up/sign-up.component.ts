import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { takeUntil } from 'rxjs/operators';
import { ReplaySubject } from 'rxjs';

import { AuthService } from '../../../services/auth.service';
import { StarterPageRoute, StarterPageType } from '../../../shared/models/starter.models';
import { Error, SignIn, SignUpActivationPayload, SignUpError } from '../../../shared/models/auth.models';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit, OnDestroy {
  private destroy$: ReplaySubject<void> = new ReplaySubject(1);
  page: StarterPageType = 'sign-up';
  route: StarterPageRoute = '/sign-in';
  form: FormGroup;
  signUpErrors: SignUpError = {
    firstName: null,
    lastName: null,
    email: null,
    password: null,
    passwordConfirm: null
  };
  notActivated = true;

  constructor(
    private fb: FormBuilder,
    private auth: AuthService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
    this.initForm();
    this.subscribeToQueryParams();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  private initForm(): void {
    this.form = this.fb.group({
      email: ['', [Validators.required, Validators.pattern('[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+')]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      passwordConfirm: ['', [Validators.required, Validators.minLength(8)]],
      firstName: ['', [Validators.required, Validators.maxLength(255)]],
      lastName: ['', [Validators.required, Validators.maxLength(255)]]
    });
  }

  private resetErrors(): void {
    this.signUpErrors = {
      firstName: null,
      lastName: null,
      email: null,
      password: null,
      passwordConfirm: null
    };
  }

  private subscribeToQueryParams(): void {
    this.activatedRoute.queryParams
      .pipe(takeUntil(this.destroy$))
      .subscribe((params: Params | SignUpActivationPayload) => {
        if (Object.keys(params).length > 0) {
          this.auth.signUpActivation(params).subscribe((res: SignIn) => {
            this.auth.setToken(res.data.attributes.accessToken);
            this.form.reset();
            this.resetErrors();
            this.router.navigate(['case-study']);
          });
        }
      });
  }

  signUp(): void {
    this.auth.signUp(this.form.value).subscribe(() => {
      this.form.reset();
      this.resetErrors();
      this.notActivated = false;
    }, err => {
      this.resetErrors();
      err.error.errors.forEach((e: Error) => {
        switch (e.source.parameter) {
          case 'firstName':
            this.signUpErrors.firstName = e.detail;
            break;
          case 'lastName':
            this.signUpErrors.lastName = e.detail;
            break;
          case 'email':
            this.signUpErrors.email = e.detail;
            break;
          case 'password':
            this.signUpErrors.password = e.detail;
            break;
          case 'passwordConfirm':
            this.signUpErrors.passwordConfirm = e.detail;
            break;
          default:
            console.error(err);
        }
      });
    });
  }
}
