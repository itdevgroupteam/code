import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from '../../../services/auth.service';
import { StarterPageRoute, StarterPageType } from '../../../shared/models/starter.models';
import { Error, SignIn, SignInError } from '../../../shared/models/auth.models';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {
  page: StarterPageType = 'sign-in';
  route: StarterPageRoute = '/sign-up';
  form: FormGroup;
  signInErrors: SignInError = {
    email: null,
    password: null,
    other: null
  };

  constructor(
    private auth: AuthService,
    private fb: FormBuilder,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.initForm();
  }

  private initForm(): void {
    this.form = this.fb.group({
      email: ['', [Validators.required, Validators.pattern('[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+')]],
      password: ['', [Validators.required, Validators.minLength(8)]]
    });
  }

  private resetErrors(): void {
    this.signInErrors = {
      email: null,
      password: null
    };
  }

  signIn(): void {
    this.auth.signIn(this.form.value).subscribe((res: SignIn) => {
      this.auth.setToken(res.data.attributes.accessToken);
      this.form.reset();
      this.resetErrors();
      this.router.navigate(['case-study']);
    }, err => {
      this.resetErrors();
      if (err.error.errors.length > 1) {
        err.error.errors.forEach((e: Error) => {
          switch (e.source.parameter) {
            case 'email':
              this.signInErrors.email = e.detail;
              break;
            case 'password':
              this.signInErrors.password = e.detail;
              break;
            default:
              console.error(err);
          }
        });
      } else {
        this.signInErrors.password = err.error.errors[0].detail;
      }
    });
  }
}
