import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuards } from './services/auth.guards';
import { UserResolver } from './resolvers/user.resolver';
import { ArticleResolver } from './resolvers/article.resolver';

import { SignUpComponent } from './components/starter/sign-up/sign-up.component';
import { AppComponent } from './app.component';
import { SignInComponent } from './components/starter/sign-in/sign-in.component';
import { PasswordRecoveryComponent } from './components/starter/password-recovery/password-recovery.component';
import { NewPasswordComponent } from './components/starter/new-password/new-password.component';
import { StudyArticleComponent } from './components/listing/study-article/study-article.component';

const routes: Routes = [
  {
    path: 'sign-up',
    component: SignUpComponent,
    canActivate: [AuthGuards]
  },
  {
    path: 'sign-in',
    component: SignInComponent,
    canActivate: [AuthGuards]
  },
  {
    path: 'password-recovery',
    component: PasswordRecoveryComponent,
    canActivate: [AuthGuards]
  },
  {
    path: 'new-password',
    component: NewPasswordComponent,
    canActivate: [AuthGuards]
  },
  {
    path: 'auth/password-recovery',
    component: NewPasswordComponent,
    canActivate: [AuthGuards]
  },
  {
    path: 'auth/activation',
    component: SignUpComponent,
    canActivate: [AuthGuards]
  },
  {
    path: 'case-study',
    children: [
      {
        path: '',
        loadChildren: () => import('./components/listing/listing.module').then(m => m.ListingModule),
        canLoad: [AuthGuards],
        resolve: {
          user: UserResolver
        }
      }
    ]
  },
  {
    path: 'study-article/:slug',
    component: StudyArticleComponent,
    resolve: {
      article: ArticleResolver
    }
  },
  {
    path: '**',
    redirectTo: '/case-study'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    scrollPositionRestoration: 'enabled'
  })],
  exports: [RouterModule],
  bootstrap: [AppComponent]
})
export class AppRoutingModule {
}
