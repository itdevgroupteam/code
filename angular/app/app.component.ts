import { Component, HostListener } from '@angular/core';
import { ScrollToService } from '@nicky-lenaers/ngx-scroll-to';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Сmt';
  toTopShow = false;

  @HostListener('window:scroll', [])
  onScroll(): void {
    this.toTopShow = window.scrollY > window.innerHeight;
  }

  constructor(
    private scrollService: ScrollToService
  ) {
  }

  scroll(): void {
    this.scrollService.scrollTo({
      target: 0,
      duration: 500,
      container: 'body'
    });
  }
}
