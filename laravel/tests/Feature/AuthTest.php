<?php

namespace Tests\Feature;

use Illuminate\Http\Response;
use Tests\TestCase;

/**
 * Class AuthTest
 * @package Tests\Feature
 */
class AuthTest extends TestCase
{
    /**
     * @test
     */
    public function shouldSignUpNewUser()
    {
        $this->json('POST', route('auth.sing-up'), [
            'name' => 'test user',
            'email' => 'test-user@test.com',
            'password' => 'secret',
            'confirm_password' => 'secret'
        ], [])
            ->assertJsonStructure([
                'type',
                'attributes' => [
                    'tokenType',
                    'token'
                ],
            ])
            ->assertStatus(Response::HTTP_OK);

        /** Check data in DB */
        $this->assertDatabaseHas(
            'users',
            [
                'name' => 'test user',
                'email' => 'test-user@test.com',
            ]
        );
    }

    /**
     * @test
     */
    public function shouldSignUpValidationError()
    {
        $this->json('POST', route('auth.sing-up'), [
            'name' => 'test user',
            'email' => 'admin@test.com',
            'password' => 'secret',
            'confirm_password' => 'password1'
        ], [])
            ->assertJson([
                [
                    'status' => 422,
                    'detail' => 'The email has already been taken.',
                    'source' => [
                        'parameter' => 'email',
                    ],
                ],
                [
                    'status' => 422,
                    'detail' => 'The confirm password and password must match.',
                    'source' => [
                        'parameter' => 'confirm_password',
                    ],
                ],
            ])
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @test
     */
    public function shouldSignInUser()
    {
        $this->json('POST', route('auth.sing-in'), [
            'email' => 'user@test.com',
            'password' => 'secret',
        ], [])
            ->assertJsonStructure([
                'type',
                'attributes' => [
                    'tokenType',
                    'token'
                ],
            ])
            ->assertStatus(Response::HTTP_OK);
    }

    /**
     * @test
     */
    public function shouldSignInValidationError()
    {
        $this->json('POST', route('auth.sing-in'), [
            'email' => 'wrong-user@test.com',
            'password' => 'secret',
        ], [])
            ->assertJson([
                'message' => 'Email or password is wrong'
            ])
            ->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    /**
     * @test
     */
    public function shouldRefreshToken()
    {
        $data = json_decode(
            $this->json('POST', route('auth.sing-in'), [
                'email' => 'user@test.com',
                'password' => 'secret',
            ], [])
                ->getContent()
        );

        $token = sprintf(
            '%s %s',
            $data->attributes->tokenType,
            $data->attributes->token,
        );

        $this->json('GET', route('auth.refresh-token'), [], [
            'Authorization' => $token
        ])
            ->assertJsonStructure([
                'type',
                'attributes' => [
                    'tokenType',
                    'token'
                ],
            ])
            ->assertStatus(Response::HTTP_OK);
    }
}
