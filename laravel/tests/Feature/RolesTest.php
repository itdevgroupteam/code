<?php

namespace Tests\Feature;

use Illuminate\Http\Response;
use Tests\TestCase;

/**
 * Class RolesTest
 * @package Tests\Feature
 */
class RolesTest extends TestCase
{
    /**
     * @test
     */
    public function shouldReturnRolesList()
    {
        $this->actingAs($this->getAdmin())
            ->json('GET', route('admin.roles.index'), [], [])
            ->assertJson([
                'data' => [
                    [
                        'type' => 'role',
                        'attributes' => [
                            'name' => 'admin',
                            'displayName' => 'Admin',
                            'description' => null,
                        ]
                    ],
                    [
                        'type' => 'role',
                        'attributes' => [
                            'name' => 'user',
                            'displayName' => 'User',
                            'description' => null,
                        ]
                    ],
                ],
            ])
            ->assertStatus(Response::HTTP_OK);
    }

    /**
     * @test
     */
    public function shouldCreateNewRole()
    {
        $this->assertDatabaseMissing('roles', ['name' => 'new-role']);

        $this->actingAs($this->getAdmin())
            ->json('POST', route('admin.roles.store'), [
                'name' => 'new-role',
                'displayName' => 'New Role',
                'description' => 'Some new role description',
            ], [])
            ->assertJson([
                'type' => 'role',
                'attributes' => [
                    'name' => 'new-role',
                    'displayName' => 'New Role',
                    'description' => 'Some new role description',
                ],
            ])
            ->assertStatus(Response::HTTP_OK);

        $this->assertDatabaseHas('roles', ['name' => 'new-role']);
    }

    /**
     * @test
     */
    public function shouldUpdateNewRole()
    {
        $res = json_decode($this->actingAs($this->getAdmin())
            ->json('POST', route('admin.roles.store'), [
                'name' => 'new-role',
                'displayName' => 'New Role',
                'description' => 'Some new role description',
            ], [])
            ->getContent()
        );

        $this->actingAs($this->getAdmin())
            ->json('PUT', route('admin.roles.update', ['id' => $res->id]), [
                'name' => 'new-role-update',
                'displayName' => 'New Role Update',
                'description' => 'Some new role description',
            ], [])
            ->assertJson([
                'type' => 'role',
                'attributes' => [
                    'name' => 'new-role-update',
                    'displayName' => 'New Role Update',
                    'description' => 'Some new role description',
                ],
            ])
            ->assertStatus(Response::HTTP_OK);

        $this->assertDatabaseHas('roles', ['name' => 'new-role-update']);
    }

    /**
     * @test
     */
    public function shouldDeleteAdditionalRole()
    {
        $res = json_decode($this->actingAs($this->getAdmin())
            ->json('POST', route('admin.roles.store'), [
                'name' => 'new-role',
                'displayName' => 'New Role',
                'description' => 'Some new role description',
            ], [])
            ->getContent()
        );

        $this->assertDatabaseHas('roles', ['name' => 'new-role']);

        $this->actingAs($this->getAdmin())
            ->json('DELETE', route('admin.roles.delete', ['id' => $res->id]), [], [])
            ->assertStatus(Response::HTTP_NO_CONTENT);

        $this->assertDatabaseMissing('roles', ['name' => 'new-role']);
    }

    /**
     * @test
     */
    public function shouldNotDeleteDefaultRole()
    {
        $this->assertDatabaseHas('roles', ['name' => 'user']);

        $this->actingAs($this->getAdmin())
            ->json('DELETE', route('admin.roles.delete', ['id' => 2]), [], [])
            ->assertJson([
                'message' => 'This role can\'t be deleted'
            ])
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $this->assertDatabaseHas('roles', ['name' => 'user']);
    }
}
