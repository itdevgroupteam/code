<?php

namespace Tests\Feature;

use Illuminate\Http\Response;
use Tests\TestCase;

/**
 * Class RbacTest
 * @package Tests\Feature
 */
class RbacTest extends TestCase
{
    /**
     * @test
     */
    public function shouldReturnErrorOnQueriedWithoutRoleAdmin()
    {
        $this->actingAs($this->getUser())
            ->json('GET', route('admin.roles.index'), [], [])
            ->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    /**
     * @test
     */
    public function shouldReturnSuccessOnQueriedWithRoleAdmin()
    {
        $this->actingAs($this->getAdmin())
            ->json('GET', route('admin.roles.index'), [], [])
            ->assertStatus(Response::HTTP_OK);
    }

    /**
     * @test
     */
    public function shouldAssignRoleAdminToUser()
    {
        $this->assertDatabaseMissing('role_user', [
            'role_id' => 1,
            'user_id' => 2
        ]);

        $this->actingAs($this->getAdmin())
            ->json('POST', route('admin.users.attach-role'), [
                'userId' => 2,
                'roleId' => 1,
            ], [])
            ->assertStatus(Response::HTTP_CREATED);

        $this->assertDatabaseHas('role_user', [
            'role_id' => 1,
            'user_id' => 2
        ]);
    }
}
