<?php

namespace Tests;

use App\Models\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

/**
 * Class TestCase
 * @package Tests
 */
abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    
    /**
     *
     */
    protected function setUp(): void
    {
        parent::setUp();
        
        $this->setupDatabase();
    }

    /**
     *
     */
    public function setupDatabase(): void
    {
        if (!file_exists(storage_path('db'))) {
            mkdir(storage_path('db'));
            chmod(storage_path('db'), 0777);
        }

        if (!file_exists(storage_path('db/stubdb.sqlite'))) {
            touch(storage_path('db/testdb.sqlite'));

            $this->artisan('migrate');
            $this->artisan('db:seed');
            $this->artisan('permissions:generate');

            chmod(storage_path('db/testdb.sqlite'), 0777);
            copy(storage_path('db/testdb.sqlite'), storage_path('db/stubdb.sqlite'));
            chmod(storage_path('db/stubdb.sqlite'), 0777);
        }

        copy(storage_path('db/stubdb.sqlite'), storage_path('db/testdb.sqlite'));
        $this->artisan('cache:clear');
    }

    /**
     *
     */
    protected function tearDown(): void
    {
        if ($this->app) {
            $connectionName = $this->app->make('config')->get('database.default');
            $this->app->make('db')->disconnect($connectionName);
        }

        parent::tearDown();
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return User::whereEmail('user@test.com')->first();
    }

    /**
     * @return User
     */
    protected function getAdmin(): User
    {
        return User::whereEmail('admin@test.com')->first();
    }
}
