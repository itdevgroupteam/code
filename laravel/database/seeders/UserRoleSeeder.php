<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Seeder;

/**
 * Class UserRoleSeeder
 * @package Database\Seeders
 */
class UserRoleSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        $roleAdmin = Role::where('name', '=', 'admin')->firstOrFail();
        $roleUser = Role::where('name', '=', 'user')->firstOrFail();

        User::chunk(10, function (Collection $users) use ($roleAdmin, $roleUser) {
            $users->each(function (User $user) use ($roleAdmin, $roleUser) {
                if ($user->name === 'admin') {
                    $user->attachRole($roleUser);
                    $user->attachRole($roleAdmin);
                } else {
                    $user->attachRole($roleUser);
                }
            });
        });
    }
}
