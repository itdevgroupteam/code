<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/sing-in', 'AuthController@signIn')
    ->name('auth.sing-in');

Route::post('/sing-up', 'AuthController@signUp')
    ->name('auth.sing-up');

Route::middleware('auth:sanctum')
    ->get('/refresh-token', 'AuthController@refreshToken')
    ->name('auth.refresh-token');

//If need add routes such as reset password, activate account e.t.c. - need to add here
