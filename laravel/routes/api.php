<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'users', 'as' => 'users.', 'middleware' => 'auth:sanctum'], function () {
    Route::get('/me', 'UserController@me')
        ->name('me');
    Route::get('/sessions', 'UserController@sessions')
        ->name('sessions');
    Route::delete('/drop-session/{id}', 'UserController@dropSession')
        ->name('drop-session');
    Route::get('/logout', 'UserController@logout')
        ->name('logout');
});
