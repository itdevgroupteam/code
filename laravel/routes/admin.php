<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//rbac
Route::group(['middleware' => ['auth:sanctum', 'rbac']], function () {
    Route::group(['as' => 'roles', 'prefix' => 'roles'], function () {
        Route::get('/', 'RoleController@index')->name('.index');
        Route::post('/', 'RoleController@store')->name('.store');
        Route::put('/{id}', 'RoleController@update')->name('.update');
        Route::delete('/{id}', 'RoleController@delete')->name('.delete');
    });
    Route::group(['as' => 'users', 'prefix' => 'users'], function () {
        Route::post('/attach-role', 'UserController@attachRole')->name('.attach-role');
        Route::post('/detach-role', 'UserController@detachRole')->name('.detach-role');
    });
});
