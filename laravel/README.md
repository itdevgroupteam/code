## Based structure

The based structure based on action classes, which do one action from business logic. On class constructor implement
dependencies, data to action set in function input. In general - action classes is aggregators. Main part of logic be in
services.

> Actions class directory - app/Actions

> Services directory - app/Services

## Deploying 

#### Manual deploy

Need to clarify next services installations:
- PHP-8.0 or newer
- nginx or apache
- Database service (MySql by default)

Steps to deploy

* Clone code to project directory
* Copy `.env.example` to `.env` and set correct variables data
* Install Composer dependencies use command `composer install ` or `composer install --no-dev` to PROD env
* Set-up DB migrations use command `php artisan migrate --seed`
* Install default permissions use command `php artisan permissions:generate`

#### Manual deploy with docker

Steps to deploy

* Clone code to project directory
* Copy `.env.example` to `.env` and set correct variables data
* Deploy docker-containers use command `docker-compose build`
* Run docker-containers use command `docker-compose up -d`
* Set up DB use command `docker-compose exec php-fpm php artisan migrate --seed`
* Set up permissions use command `docker-compose exec php-fpm php artisan permissions:generate`

##### Automatic deploy with docker

Steps to deploy

* Clone code to project directory
* Copy `.env.example` to `.env` and set correct variables data
* Using `Make` tool deploy project (command - `make`);

#### Available make commands

* make - main command. Deploy/start project on docker-container with install dependencies, migrate and RBAC set-up
* make composer-install
* make docker-build
* make docker-up
* make docker-stop
* make docker-restart
* make docker-down-orphans
* make docker-rebuild
* make run-migration
* make reset-migration
* make run-migration-seed
* make generate-model-annotation
* make generate-permissions

## Authentication
### Based auth

Based component - firebase/php-jwt  
https://packagist.org/packages/firebase/php-jwt

Service implementation provides App\Services\JwtGuard and App\Provider\JwtServiceProvider classes.  
https://laravel.com/docs/8.x/authentication#adding-custom-guards

Connection of custom components implemented in Auth service provider class

```
public function boot()
    {
        $this->registerPolicies();

        // add custom guard provider
        Auth::provider('jwt', function ($app, array $config) {
            return app(JwtServiceProvider::class);
        });

        // add custom guard
        Auth::extend('jwt', function ($app, $name, array $config) {
            return new JwtGuard(Auth::createUserProvider($config['provider']), $app->make('request'));
        });
    }
```

Jwt strategy implemented in `App\Services\JwtService`, and connect with Laravel authentication system in
`App\Provider\JwtServiceProvider::retrieveByToken` function

```
   /**
     * @param null $identifier
     * @param string $token
     *
     * @return User|Authenticatable|null
     */
    public function retrieveByToken($identifier, $token)
    {
        return $this->jwtService->authByToken($token);
    }
```

This function is used when checking the access token on requests from the user also

For a generate JWT token to other services can use `App\Services\JwtService` functions:

- `generateToken()` :
    * data `array <["iss" => $user->email, "aud" => $accessToken->token, ]>`
    * return `string <$token>`
- `getDataByToken()` :
    * data `string <$token>`
    * return `array <$tokenData>`
- `authByToken()` :
    * data `string <$token>`
    * return `User|null`

### User's sessions, sessions tokens

User session tokens are generated in SignIn/SignUp actions and saved in user_access_tokens table.
When a user is authorized by a token, the session token is compared with the existing ones, and participates in the 
generation of the Jwt token. Can see it in `App\Services\JwtService::authByToken` function.
User can remove access on each moment. Jwt tokens, which generated uses removed access token, will not work

## RBAC

RBAC system based on `https://laratrust.santigarcor.me/ ` library.

RBAC system setting save in tables:
- roles - save a list of available roles
- permission_role - mapping role to permission
- permission_user - mapping permissions to user
- permissions - list of available permissions
- role_user - mapping roles to user

Implemented Role-Permission system by default. 
In a system exists list of permissions, based on list of routes (route name). 
Permissions mapped with role, role mapped with user

1 resolution can be associated with several roles. User can have several roles.
Admin can create/remove additional roles. 
Default roles (admin, user), can't delete, but can be changed in developing process

## TESTING

For a test using sqlite DB on `storage/db/` path.

Tests based on phpunit library with standard implementations.
Tests for RBAC, Roles and Auth available.
sqlite DB can be deployed on all instances and not affect main DB storage

Settings to DB and other services for a test environment set up in phpunit.xml file. 
Please DON'T  implement other implementations for a test setings or please discuss idea before implementations.

