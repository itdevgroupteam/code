<?php

namespace App\Models;

use Laratrust\Models\LaratrustRole;

/**
 * Class Role
 * @package App\Models
 */
class Role extends LaratrustRole
{
    /** List of roles */
    /** @type string */
    const ROLE_ADMIN = 'admin';
    /** @type string */
    const ROLE_USER = 'user';

    /**
     * List of default roles. This roles can't be deleted
     * @var string[]
     */
    const DEFAULT_ROLES = [self::ROLE_ADMIN, self::ROLE_USER];

    public $guarded = [];
}
