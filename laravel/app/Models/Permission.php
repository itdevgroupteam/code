<?php

namespace App\Models;

use Laratrust\Models\LaratrustPermission;

/**
 * Class Permission
 * @package App\Models
 */
class Permission extends LaratrustPermission
{
    public $guarded = [];
}
