<?php

declare(strict_types=1);

namespace App\Contracts;

use App\Models\User;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Interface SessionServiceInterface
 * @package App\Contracts
 */
interface SessionServiceInterface
{
    /**
     * Fetch list of accesses for sessions for a current user
     *
     * @param User $user
     *
     * @return LengthAwarePaginator
     */
    public function getListOfSessions(User $user): LengthAwarePaginator;

    /**
     * Revokes/Delete access token.
     * JWT and other accesses based on revokes token will not work
     * Other sessions will work correctly
     *
     * @param User $user
     * @param int $id
     *
     * @return bool
     */
    public function dropSession(User $user, string $id): bool;
}