<?php

declare(strict_types=1);

namespace App\Contracts;

use App\Models\Role;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Arr;

/**
 * Interface RbacServiceInterface
 * @package App\Contracts
 */
interface RbacServiceInterface
{
    /**
     * @return LengthAwarePaginator
     */
    public function getList(): LengthAwarePaginator;

    /**
     * Creates a new record of role.
     * New role can be assigned to existed users
     * Existing accesses (permissions) can be assigned to new role
     *
     * @param array $data
     *
     * @return Role
     */
    public function createRole(array $data): Role;

    /**
     * Update a record of role
     * Permissions, when assigned to role and user's accesses not be changed
     *
     * @param int $roleId
     * @param array $data
     *
     * @return mixed
     */
    public function updateRole(int $roleId, array $data);

    /**
     * Removes a record of role
     * Permissions, when assigned to role, not be changed, but will not work,
     * if this accesses was not be assigned to other role
     * Users who have only removes role, will not be able to access until the new role is assigned
     *
     * @param int $id
     *
     * @return void
     */
    public function deleteRole(int $id): void;
}