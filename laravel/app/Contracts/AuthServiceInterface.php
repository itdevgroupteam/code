<?php

declare(strict_types=1);

namespace App\Contracts;

use App\Models\User;

/**
 * Interface AuthServiceInterface
 * @package App\Contracts
 */
interface AuthServiceInterface
{
    /**
     * Creates/Renews a session access token.
     * Creates new JWT token to queries according to user's credentials
     *
     * @param array $data
     *
     * @return string
     */
    public function signIn(array $data): string;

    /**
     * Creates a new user's record, session access token,
     * JWT token to queries according to new user's data
     *
     * @param array $data
     *
     * @return string
     */
    public function signUp(array $data): string;

    /**
     * Revokes/Delete current access token.
     * JWT and other accesses based on revokes token will not work.
     * Other sessions will work correctly
     *
     * @param User $user
     *
     * @return bool|null
     */
    public function logout(User $user): ?bool;

    /**
     * @param User $user
     *
     * @return string
     */
    public function generateToken(User $user): string;

    /**
     * @param User $user
     *
     * @return string
     */
    public function refreshToken(User $user): string;
}