<?php

declare(strict_types=1);

namespace App\Contracts;

/**
 * Interface UserServiceInterface
 * @package App\Contracts
 */
interface UserServiceInterface
{
    /**
     * Attach role to user.
     * Accesses, attached to role, will be available for a user
     *
     * @param int $userId
     * @param int $roleId
     *
     * @return void
     */
    public function attachRole(int $userId, int $roleId): void;

    /**
     * Detach role of user.
     * Accesses, attached to role, will not available for a user
     *
     * @param int $userId
     * @param int $roleId
     *
     * @return void
     */
    public function detachRole(int $userId, int $roleId): void;
}