<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Route;

/**
 * Class GeneratePermissions
 * @package App\Console\Commands
 */
class GenerateRolePermissions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'permissions:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make a default permissions and attach permissions to role according to route list';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        /** @var Role $roleAdmin */
        $roleAdmin = Role::whereName('admin')->first();
        /** @var Role $roleUser */
        $roleUser = Role::whereName('user')->first();

        foreach (Route::getRoutes() as $route) {
            /** @var \Illuminate\Routing\Route $route */
            if (!$route->getName()) {
                continue;
            }
            $group = explode('.', $route->getName())[0];
            if ($group == 'auth') {
                continue;
            }

            $permission = Permission::whereName($route->getName())->first();

            if ($permission) {
                continue;
            }

            $permission = Permission::create([
                'name' => $route->getName(),
                'display_name' => $route->uri()
            ]);

            if ($group == 'api') {
                $roleUser->attachPermission($permission);
                $roleAdmin->attachPermission($permission);
            } else {
                $roleAdmin->attachPermission($permission);
            }
        }

        return Command::SUCCESS;
    }
}
