<?php

namespace App\Providers;

use App\Contracts\AuthServiceInterface;
use App\Contracts\RbacServiceInterface;
use App\Contracts\SessionServiceInterface;
use App\Contracts\UserServiceInterface;
use App\Services\AuthService;
use App\Services\RbacService;
use App\Services\SessionService;
use App\Services\UserService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->isLocal()) {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }

        $this->app->singleton(AuthServiceInterface::class, AuthService::class);
        $this->app->singleton(SessionServiceInterface::class, SessionService::class);
        $this->app->singleton(UserServiceInterface::class, UserService::class);
        $this->app->singleton(RbacServiceInterface::class, RbacService::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
