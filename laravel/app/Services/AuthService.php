<?php

declare(strict_types=1);

namespace App\Services;

use App\Contracts\AuthServiceInterface;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\UnauthorizedException;

/**
 * Class AuthService
 * @package App\Services
 */
class AuthService implements AuthServiceInterface
{
    /**
     * Creates/Renews a session access token.
     * Creates new JWT token to queries according to user's credentials
     *
     * @param array $data
     *
     * @return string
     */
    public function signIn(array $data): string
    {
        /** @var User $user */
        $user = User::where('email', '=', Arr::get($data, 'email'))
            ->first();

        if (!$user || !Hash::check(Arr::get($data, 'password'), $user->password)) {
            throw new UnauthorizedException('Email or password is wrong');
        }

        return $this->generateToken($user);
    }

    /**
     * Creates a new user's record, session access token,
     * JWT token to queries according to new user's data
     *
     * @param array $data
     *
     * @return string
     */
    public function signUp(array $data): string
    {
        $roleUser = Role::whereName(Role::ROLE_ADMIN)->first();
        /** @var User $user */
        $user = User::create([
            'name' => Arr::get($data, 'name'),
            'email' => Arr::get($data, 'email'),
            'password' => Hash::make(Arr::get($data, 'email'))
        ]);
        $user->attachRole($roleUser);

        return $this->generateToken($user);
    }

    /**
     * Revokes/Delete current access token.
     * JWT and other accesses based on revokes token will not work.
     * Other sessions will work correctly
     *
     * @param User $user
     *
     * @return bool|null
     */
    public function logout(User $user): ?bool
    {
        return $user->currentAccessToken()->delete();
    }

    /**
     * @param User $user
     *
     * @return string
     */
    public function generateToken(User $user): string
    {
        //To name can add IP, client id (front-end) or other params to provide multi-sessions
        //Also you can modify `personal_access_tokens` table to add custom columns to save additional info
        $tokenName = sprintf(
            '%s-%s',
            substr($user->email, 0, strrpos($user->email, '@')),
            uniqid()
        );

        if ($user->currentAccessToken()) {
            $user->currentAccessToken()->delete();
        }

        return $user->createToken($tokenName)->plainTextToken;
    }

    /**
     * @param User $user
     *
     * @return string
     */
    public function refreshToken(User $user): string
    {
        $user->currentAccessToken()->delete();

        return $this->generateToken($user);
    }
}