<?php

declare(strict_types=1);

namespace App\Services;

use App\Contracts\SessionServiceInterface;
use App\Models\User;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class SessionService
 * @package App\Services
 */
class SessionService implements SessionServiceInterface
{
    /**
     * Fetch list of accesses for sessions for a current user
     *
     * @param User $user
     *
     * @return LengthAwarePaginator
     */
    public function getListOfSessions(User $user): LengthAwarePaginator
    {
        //TODO:: Pagination logic must be implemented according to project requirements.
        // F.e. using separate pagination class or global pagination settings
        return $user->tokens()->paginate();
    }

    /**
     * Revokes/Delete access token.
     * JWT and other accesses based on revokes token will not work
     * Other sessions will work correctly
     *
     * @param User $user
     * @param int $id
     *
     * @return bool
     */
    public function dropSession(User $user, string $id): bool
    {
        $session = $user->tokens()->findOrFail($id);

        return (bool)$session->delete();
    }
}
