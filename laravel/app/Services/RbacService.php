<?php

declare(strict_types=1);

namespace App\Services;

use App\Contracts\RbacServiceInterface;
use App\Models\Role;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Arr;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * Class RbacService
 * @package App\Services
 */
class RbacService implements RbacServiceInterface
{
    /**
     * @return LengthAwarePaginator
     */
    public function getList(): LengthAwarePaginator
    {
        return Role::paginate();
    }

    /**
     * Creates a new record of role.
     * New role can be assigned to existed users
     * Existing accesses (permissions) can be assigned to new role
     *
     * @param array $data
     *
     * @return Role
     */
    public function createRole(array $data): Role
    {
        return Role::create([
            'name' => Arr::get($data, 'name'),
            'display_name' => Arr::get($data, 'displayName'),
            'description' => Arr::get($data, 'description'),
        ]);
    }

    /**
     * Update a record of role
     * Permissions, when assigned to role and user's accesses not be changed
     *
     * @param int $roleId
     * @param array $data
     *
     * @return mixed
     */
    public function updateRole(int $roleId, array $data)
    {
        /** @var Role $role */
        $role = Role::findOrFail($roleId);

        $role->name = Arr::get($data, 'name');
        $role->display_name = Arr::get($data, 'displayName');
        $role->description = Arr::get($data, 'description');
        $role->save();

        return $role;
    }

    /**
     * Removes a record of role
     * Permissions, when assigned to role, not be changed, but will not work,
     * if this accesses was not be assigned to other role
     * Users who have only removes role, will not be able to access until the new role is assigned
     *
     * @param int $id
     *
     * @return void
     */
    public function deleteRole(int $id): void
    {
        /** @var Role $role */
        $role = Role::findOrFail($id);

        if (in_array($role->name, Role::DEFAULT_ROLES)) {
            throw new UnprocessableEntityHttpException('This role can\'t be deleted');
        }

        $role->delete();
    }
}