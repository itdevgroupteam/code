<?php

declare(strict_types=1);

namespace App\Services;

use App\Contracts\UserServiceInterface;
use App\Models\Role;
use App\Models\User;

/**
 * Class UserService
 * @package App\Services
 */
class UserService implements UserServiceInterface
{
    /**
     * Attach role to user.
     * Accesses, attached to role, will be available for a user
     *
     * @param int $userId
     * @param int $roleId
     *
     * @return void
     */
    public function attachRole(int $userId, int $roleId): void
    {
        /** @var User $user */
        $user = User::findOrFail($userId);
        /** @var Role $role */
        $role = Role::findOrFail($roleId);

        $user->attachRole($role);
    }

    /**
     * Detach role of user.
     * Accesses, attached to role, will not available for a user
     *
     * @param int $userId
     * @param int $roleId
     *
     * @return void
     */
    public function detachRole(int $userId, int $roleId): void
    {
        /** @var User $user */
        $user = User::findOrFail($userId);
        /** @var Role $role */
        $role = Role::findOrFail($roleId);

        $user->detachRole($role);
    }
}