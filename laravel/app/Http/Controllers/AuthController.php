<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Contracts\AuthServiceInterface;
use App\Http\Requests\Auth\SignInRequest;
use App\Http\Requests\Auth\SignUpRequest;
use App\Http\Resources\Auth\SignInResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class AuthController
 * @package App\Http\Controllers
 */
class AuthController extends Controller
{
    /**
     * @param AuthServiceInterface $authService
     * @param SignInRequest $request
     *
     * @return JsonResponse
     */
    public function signIn(AuthServiceInterface $authService, SignInRequest $request): JsonResponse
    {
        $token = $authService->signIn($request->all());

        return response()->json(new SignInResource([
            'tokenType' => 'Bearer',
            'token' => $token,
        ]));
    }

    /**
     * @param AuthServiceInterface $authService
     * @param SignUpRequest $request
     *
     * @return JsonResponse
     */
    public function signUp(AuthServiceInterface $authService, SignUpRequest $request): JsonResponse
    {
        $token = $authService->signUp($request->all());

        return response()->json(new SignInResource([
            'tokenType' => 'Bearer',
            'token' => $token,
        ]));
    }

    /**
     * @param AuthServiceInterface $authService
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function refreshToken(AuthServiceInterface $authService, Request $request): JsonResponse
    {
        $token = $authService->refreshToken($request->user());

        return response()->json(new SignInResource([
            'tokenType' => 'Bearer',
            'token' => $token,
        ]));
    }
}
