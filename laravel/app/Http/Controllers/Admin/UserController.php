<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Contracts\UserServiceInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\Users\AttachUserRoleRequest;
use App\Http\Requests\Users\DetachUserRoleRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

/**
 * Class UserController
 * @package App\Http\Controllers\Admin
 */
class UserController extends Controller
{
    /**
     * @param UserServiceInterface $userService
     * @param AttachUserRoleRequest $request
     *
     * @return JsonResponse
     */
    public function attachRole(UserServiceInterface $userService, AttachUserRoleRequest $request): JsonResponse
    {
        $userService->attachRole(
            (int)$request->get('userId'),
            (int)$request->get('roleId'),
        );

        return new JsonResponse(null, Response::HTTP_CREATED);
    }

    /**
     * @param DetachUserRoleRequest $request
     *
     * @return JsonResponse
     */
    public function detachRole(UserServiceInterface $userService, DetachUserRoleRequest $request): JsonResponse
    {
        $userService->detachRole(
            (int)$request->get('userId'),
            (int)$request->get('roleId'),
        );

        return new JsonResponse(null, Response::HTTP_CREATED);
    }
}
