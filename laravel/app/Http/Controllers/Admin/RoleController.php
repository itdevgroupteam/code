<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Actions\Roles\CreateRoleAction;
use App\Actions\Roles\DeleteRoleAction;
use App\Actions\Roles\GetRolesListAction;
use App\Actions\Roles\UpdateRoleAction;
use App\Contracts\RbacServiceInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\Role\RoleRequest;
use App\Http\Resources\Role\RoleResource;
use App\Http\Resources\Role\RolesListResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Class RoleController
 * @package App\Http\Controllers\Admin
 */
class RoleController extends Controller
{
    /**
     * @param RbacServiceInterface $rbacService
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function index(RbacServiceInterface $rbacService, Request $request): JsonResponse
    {
        $list = $rbacService->getList();

        return new JsonResponse(new RolesListResource($list));
    }

    /**
     * @param RbacServiceInterface $rbacService
     * @param RoleRequest $request
     *
     * @return JsonResponse
     */
    public function store(RbacServiceInterface $rbacService, RoleRequest $request): JsonResponse
    {
        $role = $rbacService->createRole($request->all());

        return new JsonResponse(new RoleResource($role));
    }

    /**
     * @param RbacServiceInterface $rbacService
     * @param RoleRequest $request
     * @param int $id
     *
     * @return JsonResponse
     */
    public function update(RbacServiceInterface $rbacService, RoleRequest $request, int $id): JsonResponse
    {
        $role = $rbacService->updateRole($id, $request->all());

        return new JsonResponse(new RoleResource($role));
    }

    /**
     * @param RbacServiceInterface $rbacService
     * @param int $id
     *
     * @return JsonResponse
     */
    public function delete(RbacServiceInterface $rbacService, int $id): JsonResponse
    {
        $rbacService->deleteRole($id);

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
