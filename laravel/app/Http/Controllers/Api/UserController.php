<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Contracts\AuthServiceInterface;
use App\Contracts\SessionServiceInterface;
use App\Http\Resources\UserAccessToken\UserAccessTokenListResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class UserController
 * @package App\Http\Controllers\Api
 */
class UserController
{
    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function me(Request $request): JsonResponse
    {
        return new JsonResponse($request->user());
    }

    /**
     * @param SessionServiceInterface $sessionService
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function sessions(SessionServiceInterface $sessionService, Request $request): JsonResponse
    {
        $list = $sessionService->getListOfSessions($request->user());

        return new JsonResponse(new UserAccessTokenListResource($list));
    }

    /**
     * @param SessionServiceInterface $sessionService
     * @param Request $request
     * @param $id
     *
     * @return JsonResponse
     */
    public function dropSession(SessionServiceInterface $sessionService, Request $request, string $id): JsonResponse
    {
        $sessionService->dropSession($request->user(), $id);

        return new JsonResponse(null, 204);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function logout(AuthServiceInterface $authService, Request $request): JsonResponse
    {
        $authService->logout($request->user());

        return new JsonResponse(null, 204);
    }
}
