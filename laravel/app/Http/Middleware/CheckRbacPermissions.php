<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Validation\UnauthorizedException;

/**
 * Checking a permission to route according to user's role and permissions
 *
 * Class CheckRbacPermissions
 * @package App\Http\Middleware
 */
class CheckRbacPermissions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->user()->isAbleTo($request->route()->getName())){
            return $next($request);
        }

        throw new UnauthorizedException('Not authorized');
    }
}
