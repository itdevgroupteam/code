<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Validation\UnauthorizedException;

/**
 * Class Auth
 * @package App\Http\Middleware
 */
class Auth
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @param string $role
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        /** @var User $user */
//        $user = $request;
//        dd($user);
//        if ($user && $user->hasRole(explode(':', $role))) {
            return $next($request);
//        }
//
//        throw new UnauthorizedException('Not authorized');
    }
}
