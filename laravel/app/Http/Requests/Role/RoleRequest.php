<?php

declare(strict_types=1);

namespace App\Http\Requests\Role;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class RoleRequest
 * @package App\Http\Requests\Role
 */
class RoleRequest extends FormRequest
{
    /**
     * @return \string[][]
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'displayName' => ['required', 'string'],
            'description' => ['nullable', 'string'],
        ];
    }
}
