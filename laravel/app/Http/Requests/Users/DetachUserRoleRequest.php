<?php

declare(strict_types=1);

namespace App\Http\Requests\Users;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class DetachUserRoleRequest
 * @package App\Http\Requests\Users
 */
class DetachUserRoleRequest extends FormRequest
{
    /**
     * @return string[][]
     */
    public function rules(): array
    {
        return [
            'userId' => ['required', 'exists:users,id'],
            'roleId' => ['required', 'exists:roles,id'],
        ];
    }
}
