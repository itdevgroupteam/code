<?php

declare(strict_types=1);

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class BaseCollectionResource
 * @package App\Http\Resourses
 */
abstract class BaseCollectionResource extends ResourceCollection
{
    /** @var array|null $pagination */
    protected $pagination;

    /**
     * BaseCollectionResource constructor.
     *
     * @param $resource
     */
    public function __construct($resource)
    {
        if ($resource instanceof LengthAwarePaginator) {
            $this->setPagination($resource);
        }

        parent::__construct($resource);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $response = [];

        $response['data'] = $this->collection->map(function ($item) {
            return $this->getItemData($item);
        });

        if ($pagination = $this->getPagination()) {
            $response['pagination'] = $pagination;
        }

        return $response;
    }

    /**
     * @param $item
     * @return array
     */
    abstract protected function getItemData($item): array;

    /**
     * @return array|null
     */
    protected function getPagination(): ?array
    {
        return $this->pagination;
    }

    /**
     * @param array|null $pagination
     */
    protected function setPagination(LengthAwarePaginator $pagination): void
    {
        $this->pagination = [
            'currentPage' => $pagination->currentPage(),
            'perPage' => $pagination->perPage(),
            'total' => $pagination->total(),
            'lastPage' => $pagination->lastPage(),
        ];
    }
}
