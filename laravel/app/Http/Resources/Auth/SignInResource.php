<?php

declare(strict_types=1);

namespace App\Http\Resources\Auth;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class SignInResource
 * @package App\Http\Resources\Auth
 */
class SignInResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'type' => 'authData',
            'attributes' => [
                'tokenType' => $this['tokenType'],
                'token' => $this['token'],
            ]
        ];
    }
}
