<?php

declare(strict_types=1);

namespace App\Http\Resources\UserAccessToken;

use App\Http\Resources\BaseCollectionResource;
use Laravel\Sanctum\PersonalAccessToken;

/**
 * Class UserAccessTokenListResource
 * @package App\Http\Resources\UserAccessToken
 */
class UserAccessTokenListResource extends BaseCollectionResource
{
    /**
     * @param PersonalAccessToken $item
     *
     * @return array
     */
    protected function getItemData($item): array
    {
        return [
            'id' => $item->id,
            'type' => 'user-access',
            'attributes' => [
                'name' => $item->name,
                'token' => '**' . mb_strcut($item->token, 2, 5) . '*********',
                'lastUsedAt' => $item->last_used_at,
                'createdAt' => $item->created_at,
                'updatedAt' => $item->updated_at,
            ]
        ];
    }
}
