<?php

declare(strict_types=1);

namespace App\Http\Resources\Role;

use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use JsonSerializable;

/**
 * Class RoleResource
 * @package App\Http\Resources\Role
 */
class RoleResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array|JsonSerializable
     */
    public function toArray($request): array
    {
        /** @var Role $this */
        return [
            'id' => $this->id,
            'type' => 'role',
            'attributes' => [
                'name' => $this->name,
                'displayName' => $this->display_name,
                'description' => $this->description,
                'createdAt' => $this->created_at,
                'updatedAt' => $this->updated_at,
            ]
        ];
    }
}
