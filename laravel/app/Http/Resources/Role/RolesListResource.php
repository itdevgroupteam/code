<?php

declare(strict_types=1);

namespace App\Http\Resources\Role;

use App\Http\Resources\BaseCollectionResource;
use App\Models\Role;

/**
 * Class RolesListResource
 * @package App\Http\Resources\Role
 */
class RolesListResource extends BaseCollectionResource
{
    /**
     * @param Role $item
     *
     * @return array
     */
    protected function getItemData($item): array
    {
        return [
            'id' => $item->id,
            'type' => 'role',
            'attributes' => [
                'name' => $item->name,
                'displayName' => $item->display_name,
                'description' => $item->description,
                'createdAt' => $item->created_at,
                'updatedAt' => $item->updated_at,
            ]
        ];
    }
}
