<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Validation\UnauthorizedException;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Throwable;

/**
 * Class Handler
 * @package App\Exceptions
 */
class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var string[]
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var string[]
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
//        $this->reportable(function (Throwable $e) {
//            dd($e);
//        });
    }

    /**
     * Render an Throwable into an HTTP response.
     *
     * @param Request $request
     * @param Throwable $throwable
     * @return JsonResponse|Response|\Symfony\Component\HttpFoundation\Response
     */
    public function render($request, Throwable $throwable)
    {
        if ($throwable instanceof UnauthorizedException) {
            return response()->json(
                [
                    'message' => $throwable->getMessage()
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }

        if ($throwable instanceof UnprocessableEntityHttpException) {
            return response()->json(
                [
                    'message' => $throwable->getMessage()
                ],
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if ($throwable instanceof ValidationException) {
            /** @var ValidationException $exception */
            $errors = [];
            foreach ($throwable->errors() as $key => $error) {
                Collection::make($error)->each(function (string $message) use ($key, &$errors) {
                    $errors[] = [
                        'status' => Response::HTTP_UNPROCESSABLE_ENTITY,
                        'detail' => $message,
                        'source' => [
                            'parameter' => $key
                        ]
                    ];
                });
            };
            return (new JsonResponse($errors, Response::HTTP_UNPROCESSABLE_ENTITY));
        }

        return response()->json(
            [
                'message' => $throwable->getMessage(),
                'trace' => app()->isLocal() ? $throwable->getTrace() : null
            ],
            Response::HTTP_NOT_FOUND
        );
    }
}
